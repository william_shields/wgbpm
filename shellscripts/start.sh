#!/bin/bash
# Usage: sbatch start.sh

# set the number of nodes and processes per node - max is 10
##SBATCH --nodes=1
#sbatch --nodes=1

# set the number of tasks (processes) per node.
#sbatch --ntasks-per-node=1

# set the number of cpus used per process - max is 20
#sbatch --cpus-per-task=4

# set max wallclock time
#sbatch --time=01:00:00

# set name of job
#sbatch --job-name=wgTest

# mail alert at start, end and abortion of execution
#SBATCH --mail-type=ALL

# send mail to this address
#SBATCH --mail-user=william.shields@rhul.ac.uk

#module load OpenMPI
#module load OpenMPI/2.1.0-GCC-6.3.0-2.28
module load OpenMPI/2.1.2-GCC-6.4.0-2.28

#export CMD='echo Hello'

#mpirun $MPI_HOSTS $CMD

mpirun --mca btl_tcp_if_include eno1 $GDFIDL_HOME/Linux-x86_64/single.ompi212-gd1-$GDFIDL_VERSION < wgTest.gdf | tee LogFile-wgTest-$GDFIDL_VERSION

exit 0


