import os as _os
import numpy as _np
import time
import pickle as _pick
from string import Template as _Tem
import pandas as _pan
from copy import deepcopy
import glob as _glob

import matplotlib
matplotlib.use('pdf',warn=False, force=True)
import matplotlib.pyplot as _plt

from . import Data


def GetActiveJobs(hpcUserName='wshields'):
    """ Get number of active jobs for a given user"""
    _os.system("squeue -u " + hpcUserName + " | wc -l > jobs.txt")
    numjobs = [int(line.strip()) - 1 for line in open("jobs.txt", 'r')][0]
    _os.remove("jobs.txt")
    return numjobs


def mkdir(directory, recover=None):
    """ Make a directory if it doesn't already exist. Recovery check is to stop any directory creation
        in the event that there's an error during job submission/ """
    if recover is None:
        if not _os.path.isdir(directory):
            _os.mkdir(directory)


def Print(printout, verbose=False):
    if verbose:
        print(printout + "\n")


def CheckTrailingSlash(dirstring):
    """Check if a file path has a trailing / and add one if not."""
    if dirstring[-1] == "/":
        return dirstring
    else:
        return dirstring + "/"


def GetSpacingString(data):
    c = []
    for j in _np.spacing(data):
        if j > _np.finfo(_np.float64()).eps:
            c.append(True)
        else:
            c.append(False)
    if True in c:
        step = str(_np.mean(_np.spacing(data))) + " +/- " + str(_np.std(_np.spacing(data)))
    else:
        step = '%.3e' % (data[1] - data[0])
    return step


def WaitForJobsToEnd(hpcUserName='wshields'):
    """ Wait until all submitted jobs have been completed. """
    activeJobs = True
    while activeJobs:
        numjobs = GetActiveJobs(hpcUserName=hpcUserName)
        if numjobs == 0:
            activeJobs = False
            time.sleep(5)
        else:
            continue
    if _os.path.exists("jobs.txt"):
        _os.remove("jobs.txt")  # remove to be safe


def SaveJobs(batchjob, filename='batchjob.p'):
    """ Save list of job instances to pickle file. """
    folder = batchjob.batchresdir + "/"
    _pick.dump(batchjob, open(folder + filename, "wb"))


def GetPickledData(batchjob):
    """ Get list of pickle files in batch results data directory """
    return _glob.glob(batchjob.batchresdir + "/data/" + '*.p')


def CheckJobEndedInRecovery(jobdata):
    """ Check if a job had been already been submitted during recovery. """
    picklefile = jobdata.jobname + ".p"
    if picklefile in _os.listdir(jobdata.batchinfo.batchresdir + "/data/"):
        return True
    else:
        return False


def CheckDataIs2D(data):
    if not isinstance(data, Data.Data2D):
        raise TypeError("data is not a Data.Data2D instance")


def CheckDataIs1D(data):
    if not isinstance(data, Data.Data1D):
        raise TypeError("data is not a Data.Data1D instance")


def LoadPickleData(pfile):
    return _pick.load(open(pfile, "rb"))


def LoadFile(filename, skiprows=8):
    data = _pan.read_fwf(filename, skiprows=skiprows, delimiter=' ')
    x = data[data.keys()[0]]  # frequency
    y = data[data.keys()[1]]  # power spectrum
    return x, y


def CopyBatchInfoForJob(batchjob, jobname):
    # copy batch structure info and update for a single job
    batchinfo = deepcopy(batchjob)
    batchinfo.SetDataDir(jobname)
    return batchinfo

def WriteTemplatedFile(templateFile, outputScript, variableDict):

    with open(templateFile, "r+") as f:
        template_contents = f.read()
    template = _Tem(template_contents)

    template_resolved = template.safe_substitute(variableDict)

    with open(outputScript, "w") as f:
        f.write(template_resolved)


def CheckValTested(testedValues, randomValue):
    return randomValue in testedValues


def LoadTestedValues(filename):
    testedVals = _np.loadtxt(filename, delimiter=' ')
    valList = [list(i) for i in testedVals]
    return valList


def WriteValsAsTested(testedValsFile, values):
    writestr = "%.4f" % values[0] + " " + "%.4f" % values[1] + "\n"
    testedVals = open(testedValsFile, 'a')
    testedVals.write(writestr)
    testedVals.close()


######### General Plotting Functions

def trim_axs(axs, N):
    """ Reduce *axs* to *N* Axes. All further Axes are removed from the figure. """
    axs = axs.flat
    for ax in axs[N:]:
        ax.remove()
    return axs[:N]


def SignalDetectorIndex(data, windowsize=10, signalThreshold=1e-12):
    """ Detect the start of a time domain signal."""
    for i in range(len(data[:-(windowsize + 1)])):
        signal = _np.average(data[i:i + windowsize])
        if signal > signalThreshold:
            return i
    return 0


def GetNumFigsAndRemainder(ydata, subfigsperplot):
    remainder = _np.mod(len(ydata), subfigsperplot)
    numfigs = int(((len(ydata) - remainder) / subfigsperplot)) + 1
    return remainder, numfigs


def HideAxis(ax):
    """Hide axis lines"""
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.spines["bottom"].set_visible(False)
    ax.spines["left"].set_visible(False)
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    return ax


def GetTargetFreqArrays(data, targetFreq):
    """ Create X & Y data arrays for plotting target frequency line"""
    target_line_x = _np.ones(10) * targetFreq
    if isinstance(data, Data.Data1D) or isinstance(data, Data.Data2D):
        target_line_y = _np.linspace(data.minspec, data.maxspec, 10)
    else:
        target_line_y = _np.linspace(min(data), max(data), 10)
    return target_line_x, target_line_y


def Cut2DFreqData(data, lowerfreq, upperfreq, dataarray):
    """ Cut frequency domain data between lowerfreq and upperfreq. dataarray must be a 2d array with each row
        containing a spectrum."""
    lowerindex = _np.searchsorted(data.freq, lowerfreq)
    upperindex = _np.searchsorted(data.freq, upperfreq)
    cutdata = dataarray[:, lowerindex:upperindex]
    cutfreq = data.freq[lowerindex:upperindex]
    return cutfreq, cutdata


def Cut1DFreqData(data, lowerfreq, upperfreq, dataarray):
    """ Cut frequency domain data between lowerfreq and upperfreq. dataarray must be a 1d array containing the
        spectrum to be cut."""
    lowerindex = _np.searchsorted(data.freq, lowerfreq)
    upperindex = _np.searchsorted(data.freq, upperfreq)
    cutfreq = data.freq[lowerindex:upperindex]
    cutdata = dataarray[lowerindex:upperindex]
    return cutfreq, cutdata


def Cut2DTimeData(data, uppertime, dataarray):
    """ Cut time domain data. lowertime calculated using signal detection function. dataarray must be
        a 2d array with each row containing a signal to be cut."""
    sigindexes = [SignalDetectorIndex(i) for i in dataarray]
    lowerindex = min(sigindexes)
    upperindex = _np.searchsorted(data.time, uppertime)
    cutdata = dataarray[:, lowerindex:upperindex]
    cuttime = data.time[lowerindex:upperindex]
    return cuttime, cutdata


def Cut1DTimeData(data, uppertime, dataarray):
    """ Cut time domain data. lowertime calculated using signal detection function. dataarray must be
        a 1d array containing the spectrum to be cut."""
    lowerindex = SignalDetectorIndex(dataarray)
    upperindex = _np.searchsorted(data.time, uppertime)
    cuttime = data.time[lowerindex:upperindex]
    cutdata = dataarray[lowerindex:upperindex]
    return cuttime, cutdata


def GetSingleSpectrum(data, spec=None, param1index=None, param2index=None, band=False):
    """ Get a single spectrum, if one is supplied with the argument spec then it is simply returned, otherwise it
        is extracted from data.twod_specs
        data:        wgbpm.Data.Data1D or wgbpm.Data.Data2D instance
        param1index: if spectrum is not supplied, param1index is the index of the spectrum in the Data1D
                     instance or the first index in a Data2D instance
        param2index: second index if data is a Data2D instance
        band:        bool to extract band spectrum instead (from data.twod_band_specs array)
        """
    if spec is not None:
        return spec
    else:
        if param1index is None and param2index is None:
            raise ValueError("no index supplied for selecting spectrum in data instance")
        if isinstance(data, Data.Data1D):
            if param1index is None:
                raise ValueError("no index supplied for selecting spectrum in Data1D instance")
            elif band:
                return data.twod_band_specs[param1index]
            else:
                return data.twod_specs[param1index]
        elif isinstance(data, Data.Data2D):
            if param1index is None or param2index is None:
                raise ValueError("both indices must be supplied for selecting spectrum in Data2D instance")
            elif band:
                return data.twod_band_specs[param1index][param2index]
            else:
                return data.twod_specs[param1index][param2index]
        else:
            raise TypeError("data must be a wgbpm.Data.Data1D or wgbpm.Data.Data2D instance")


def GetTwoDSpectrum(data, spec=None, param1index=None, band=False):
    """ Get a 2d array of spectra, if one is supplied with the argument spec then it is simply returned, otherwise it
        is extracted from data.twod_specs
        data:        wgbpm.Data.Data1D or wgbpm.Data.Data2D instance
        param1index: if spectrum is not supplied, param1index is the index of the first varied parameter in the
                     Data2D instance
        band:        bool to extract band spectrum instead (from data.twod_band_specs array)
        """
    if spec is not None:
        return spec
    else:
        if isinstance(data, Data.Data2D):
            if param1index is None:
                raise ValueError("no index supplied for selecting 2D spectrum in Data2D instance")
            elif band:
                return data.twod_band_specs[param1index]
            else:
                return data.twod_specs[param1index]
        elif isinstance(data, Data.Data1D):
            if band:
                return data.twod_band_specs
            else:
                return data.twod_specs
        else:
            raise TypeError("data must be a wgbpm.Data.Data1D or wgbpm.Data.Data2D instance")


def GetSingleSignal(data, signal=None, param1index=None, param2index=None):
    """ Get a single time domain signal, if one is supplied with the argument signal then it is simply returned,
        otherwise it is extracted from data.twod_volts
        data:        wgbpm.Data.Data1D or wgbpm.Data.Data2D instance
        param1index: if signal is not supplied, param1index is the index of the signal in the Data1D
                     instance or the first index in a Data2D instance
        param2index: second index if data is a Data2D instance
        """
    if signal is not None:
        pass
    else:
        if param1index is None and param2index is None:
            raise ValueError("no index supplied for selecting signal in data instance")
        if isinstance(data, Data.Data1D):
            if param1index is None:
                raise ValueError("no index supplied for selecting signal in Data1D instance")
            else:
                return data.twod_volts[param1index]
        elif isinstance(data, Data.Data2D):
            if param1index is None or param2index is None:
                raise ValueError("both indices must be supplied for selecting signal in Data2D instance")
            else:
                return data.twod_volts[param1index][param2index]
        else:
            raise TypeError("data must be a wgbpm.Data.Data1D or wgbpm.Data.Data2D instance")
    return signal

def GetTwoDSignal(data, signal=None, param1index=None):
    """ Get a 2d array of siganl, if one is supplied with the argument signal then it is simply returned, otherwise it
        is extracted from data.twod_volts
        data:        wgbpm.Data.Data1D or wgbpm.Data.Data2D instance
        param1index: if spectrum is not supplied, param1index is the index of the first varied parameter in the
                     Data2D instance
        band:        bool to extract band spectrum instead (from data.twod_band_specs array)
        """
    if signal is not None:
        return signal
    else:
        if isinstance(data, Data.Data2D):
            if param1index is None:
                raise ValueError("no index supplied for selecting 2D signal in Data2D instance")
            else:
                return data.twod_volts[param1index]
        elif isinstance(data, Data.Data1D):
            return data.twod_volts
        else:
            raise TypeError("data must be a wgbpm.Data.Data1D or wgbpm.Data.Data2D instance")


def SaveFreqFig(title="", xscale="lin", yscale="lin", ext=".png", quickplot=False):
    """ Save a frequency domain figure.
        title: plot filename title e.g. "WaveguideWidth_0.01m"
        xscale: 'lin' or 'log'
        yscale: 'lin' or 'log'
        ext: File extension e.g. ".png"
        """
    basedir = "plots/"
    if quickplot:
        basedir = "quickplots/"
    filename = basedir + title + "_" + xscale + yscale
    fname = filename.replace(".", "_")
    _plt.savefig(fname + ext, dpi=200)


def SaveFreqFigScaled(basedir="", title="", xscale="lin", yscale="lin", ext=".png", quickplot=False):
    """ Save a frequency domain figure in the correct axis scale directory (i.e. lin or log).
        basedir: base directory name e.g "full/", "band/". Should include trailing / if specified.
        title:   plot filename title e.g. "WaveguideWidth_0.01m"
        xscale:  'lin' or 'log'
        yscale:  'lin' or 'log'
        ext:     file extension e.g. ".png"
        """
    basedir = CheckTrailingSlash(basedir)
    if yscale == "lin":
        folder = "lin/"
    else:
        folder = "log/"

    filename = basedir + folder + title
    fname = filename.replace(".","_")
    SaveFreqFig(fname, xscale, yscale, ext, quickplot)


def SaveTimeFig(title, ext=".png", quickplot=False):
    """ Save a time domain figure.
        title: plot filename title e.g. "WaveguideWidth_0.01m"
        ext:   file extension e.g. ".png"
        """
    basedir = "plots/"
    if quickplot:
        basedir = "quickplots/"
    filename = basedir + "times/" + title
    fname = filename.replace(".", "_")
    _plt.savefig(fname + ext, dpi=200)


def GetFigureScales(xscale, yscale):
    if xscale == "lin":
        xscal = "linear"
    elif xscale == "log":
        xscal = xscale
    else:
        raise ValueError("Unknown scale for x-axis: "+xscale)
    if yscale == "lin":
        yscal = "linear"
    elif yscale == "log":
        yscal = yscale
    else:
        raise ValueError("Unknown scale for y-axis: "+yscale)
    return xscal, yscal