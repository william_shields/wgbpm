import numpy as np
import matplotlib
matplotlib.use('pdf', warn=False, force=True)
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib.ticker import ScalarFormatter
import matplotlib.ticker as mtick

from .General import *

y_formatter = matplotlib.ticker.ScalarFormatter(useOffset=False)


def PlotSetup(directory):
    """ Setup plotting directories."""
    mkdir(directory+'/plots/')
    mkdir(directory+'/plots/times/')
    mkdir(directory+'/plots/band/')
    mkdir(directory+'/plots/band/lin')
    mkdir(directory+'/plots/band/log')
    mkdir(directory+'/plots/full/')
    mkdir(directory+'/plots/full/lin')
    mkdir(directory+'/plots/full/log')


def SpectrumGrid(data, variedparamname, variedparamvalues, param1index=None, param1name=None,
                 xscale='log', yscale='log', outfile='', lowerfreq=5e9, upperfreq=50e9, targetfreq=15e9,
                 subfigsperplot=9, figsize=(9,9), legendScaling=1000, fScaling=1e-9, xlabelUnit="GHz"):
    """ In a batch job where one or two geometry parameters are varied, plot a grid of subfigures each containing
        a single spectrum. If one parameter is varied, simply plot the spectra. If two parameters are varied, an
        index for a specific value of the first parameter must be supplied, with all spectra for the second varied
        parameter being plotted.
        data:              wgbpm.Data.Data1D or wgbpm.Data.Data2D instance
        variedparamname:   used in the output file name, should be the name of the varied geometry parameter,
                           e.g "WaveguideWidth".
        variedparamvalues: all values of the varied geometry parameter
        param1index:       index of the first varied geometry parameter if data is a Data2D instance
        param1name:        used in the output file name if data is a Data2D instance. Should be the name and value of
                           the first varied geometry parameter, e.g. "WaveguideHeight_0.01m"
        xscale:            'lin' or 'log'
        yscale:            'lin' or 'log'
        lowerfreq:         lower frequency limit (Hz)for all subfigures
        upperfreq:         upper frequency limit (Hz) for all subfigures
        targetfreq:        optionally draw a dashed line at a target frequency (Hz). Set to 0 if no line wanted.
        subfigsperplot:    number of subfigures per plot
        figsize:           figure size
        legendScaling:     scaling factor for the geometry value in the legend. Default=1000
        fScaling:          frequency scaling factor. Default=1e-9 (Hz to GHz)
        xlabelUnit:        frequency unit in the xlabel. Default="GHz"
        """
    if isinstance(data, Data.Data1D):
        spectra = data.twod_specs
        outfilename = 'SpectrumGrid_' + variedparamname
    elif isinstance(data, Data.Data2D):
        if param1index is None:
            raise ValueError("data is a Data2D instance, an index for a value of the first varied "
                             "geometry parameter must be supplied")
        if param1name is None:
            raise ValueError("data is a Data2D instance, a name the first varied geometry parameter must be supplied")
        spectra = data.twod_specs[param1index]
        outfilename = 'SpectrumGrid_' + param1name + "_" + variedparamname
    else:
        raise TypeError("data is not a wgbpm.Data.Data1D or wgbpm.Data.Data2D instance")

    if outfile != '':
        outfilename += '_' + outfile

    # apply frequency cuts to the data
    cutfreq, cutdata = Cut2DFreqData(data, lowerfreq, upperfreq, spectra)

    targetx, targety = GetTargetFreqArrays(data, targetfreq*fScaling)  # the 15 GHz target line
    numrnc = int(np.ceil(np.sqrt(subfigsperplot)))  # number of rows and columns in a subfigure
    remainder, numfigs = GetNumFigsAndRemainder(variedparamvalues, subfigsperplot)  # number of subfigs

    for i in range(numfigs):
        datatoplot = cutdata[i*subfigsperplot: i*subfigsperplot + subfigsperplot, :]
        fig1, axs = plt.subplots(numrnc, numrnc, figsize=figsize)

        # turn off axes not on bottom row or left-most column
        for col in range(1, numrnc):
            for row in range(0, numrnc-1):
                axs[row, col].xaxis.set_visible(False)
            for row in range(0, numrnc):
                axs[row, col].yaxis.set_visible(False)
        for row in range(0, numrnc - 1):
            for col in range(0, numrnc):
                axs[row, col].xaxis.set_visible(False)

        axs = trim_axs(axs, subfigsperplot)
        indexes = range(len(axs))
        for index, ax in zip(indexes, axs):
            if (i == numfigs - 1) and (index >= remainder):
                ax = HideAxis(ax)
            else:
                leg = variedparamname+ '= %.4f' % (legendScaling*variedparamvalues[i*subfigsperplot + index])
                if xscale == 'lin' and yscale == 'lin':
                    ax.plot(cutfreq*fScaling, datatoplot[index], 'x', ls='-', ms=2, label=leg)
                elif xscale == 'log' and yscale == 'lin':
                    ax.semilogx(cutfreq*fScaling, datatoplot[index], 'x', ls='-', ms=2, label=leg)
                elif xscale == 'lin' and yscale == 'log':
                    ax.semilogy(cutfreq*fScaling, datatoplot[index], 'x', ls='-', ms=2, label=leg)
                else:
                    ax.loglog(cutfreq*fScaling, datatoplot[index], 'x', ls='-', ms=2, label=leg)

                ax.xaxis.set_ticks(np.arange(lowerfreq*fScaling, upperfreq*fScaling + 5, 5))
                if targetfreq > 0:
                    ax.plot(targetx, targety , color='k', linestyle='--', alpha=0.5)
                ax.set_ylim(data.minspec, data.maxspec)
                ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.0e'))
                ax.legend(fontsize=8)
                ax.set_xlabel("Frequency " + xlabelUnit)
                ax.set_ylabel('Power (Arb. Units)')
        plt.tight_layout()
        SaveFreqFigScaled('full', outfilename +  '_' + str(i), xscale, yscale)
        plt.close('all')


def SingleSpectrum(data, spectrum=None, param1index=None, param2index=None, outfile='', xscale='lin', yscale='lin',
               band='wide', lowerfreq=1e9, upperfreq=50e9, targetfreq=15e9, fScaling=1e-9, xlabelUnit="GHz"):
    """ Plot a single spectrum generated by Gdfidl.
        data:        wgbpm.Data.Data1D or wgbpm.Data.Data2D object
        spectrum:    array containing spectra. Taken from data instance if not supplied.
        param1index: if spectrum is not supplied, param1index is the index of varied geometry parameter in Data1D
                     instance or index of first varied geometry parameter in Data2D instance
        param2index: index of second varied geometry parameter in Data2D instance if spectrum is not supplied
        outfile:     string for the output file name
        xscale:      'lin' or 'log'
        yscale:      'lin' or 'log'
        band:        the spectrum range plotted, either "full" for the whole spectrum, "wide" between the specified
                     lowerfreq and upperfreq from the full spectrum, or "narrow" for the Data1D or Data2D band spectrum.
        lowerfreq:   lower frequency limit (Hz) in "wide" band
        upperfreq:   upper frequency limit (Hz) in "wide" band
        targetfreq:  optionally draw a dashed line at a target frequency (Hz). Set to 0 if no line wanted.
        fScaling:    frequency scaling factor. Default=1e-9 (Hz to GHz)
        xlabelUnit:  frequency unit in the xlabel. Default="GHz"
        """
    if band == 'full':
        freq = data.freq
        spec = GetSingleSpectrum(data, spectrum, param1index, param2index)
        banddir = 'full/'
    elif band == 'wide':
        freq, spec = Cut1DFreqData(data, lowerfreq, upperfreq, spectrum)
        banddir = 'full/'
    elif band == 'narrow':
        freq = data.bandfreq
        spec = GetSingleSpectrum(data, spectrum, param1index, param2index, band=True)
        banddir = 'band/'
    else:
        raise ValueError("Unknown band: " + band + ". Only bands allowed are 'full', 'wide', and 'narrow' ")

    targetx, targety = GetTargetFreqArrays(data, targetfreq*fScaling)  # line at the target frequency

    plt.figure()
    if xscale == 'lin' and yscale == 'lin':
        plt.plot(freq*fScaling, spec)
    elif xscale == 'log' and yscale == 'lin':
        plt.semilogx(freq*fScaling, spec)
    elif xscale == 'lin' and yscale == 'log':
        plt.semilogy(freq*fScaling, spec)
    else:
        plt.loglog(freq*fScaling, spec)
    if targetfreq > 0:
        plt.plot(targetx, targety, color='k', linestyle='--', alpha=0.5)
    plt.xlabel("Frequency "+xlabelUnit)
    plt.ylabel('Power (Arb. Units)')
    plt.ylim(data.minspec, data.maxspec)
    plt.tight_layout()
    SaveFreqFigScaled(basedir=banddir, title=outfile, xscale=xscale, yscale=yscale)
    plt.close()


def SingleBandSpectrumBand(data, spectrum=None, param1index=None, param2index=None, outfile='',
                           xscale='lin', yscale='lin', targetfreq=15e9, fScaling=1e-9, xlabelUnit="GHz"):
    """ Plot a single band spectrum in a wgbpm.Data.Data1D instance. Calls SingleSpectrum method with band="narrow".
        data:        wgbpm.Data.Data1D or wgbpm.Data.Data2D object
        spectrum:    array containing spectra. Taken from data.twod_band_spec if not supplied.
        param1index: index of varied parameter in Data1D instance or index of first varied parameter in Data2D instance
        param2index: index of second varied parameter in Data2D instance
        outfile:     string for the output file name
        xscale:      'lin' or 'log'
        yscale:      'lin' or 'log'
        targetfreq:  optionally draw a dashed line at a target frequency (Hz). Set to 0 if no line wanted.
                     Must be unscaled - fScaling factor is applied to targetfreq within this function
        fScaling:    frequency scaling factor. Default=1e-9 (Hz to GHz)
        xlabelUnit:  frequency unit in the xlabel. Default="GHz"
        """
    SingleSpectrum(data, spectrum, param1index, param2index, outfile, xscale, yscale,
                   band='narrow', targetfreq=targetfreq, fScaling=fScaling, xlabelUnit=xlabelUnit)


def AllSpectra1Param1D(data, param1values, param1name, xscale='lin', yscale='lin', band=False, fScaling=1e-9,
                       xlabelUnit="GHz"):
    """ In a batch job where one geometry parameter is varied, individually plot all spectra for every value
        of that parameter.
        data:         wgbpm.Data.Data1D object containing array of spectra
        param1values: all values of the varied parameter
        param1name:   name of the varied parameter, e.g "WaveguideWidth". Is used in the output file, is
                      appended with appropriate value from param1values for each plot.
        xscale:       'lin' or 'log'
        yscale:       'lin' or 'log'
        band:         optionally plot the band spectrum rather than the full spectrum, default is False
        fScaling:     frequency scaling factor. Default=1e-9 (Hz to GHz)
        xlabelUnit:   frequency unit in the xlabel. Default="GHz"
        """
    CheckDataIs1D(data)

    for i in range(len(param1values)):
        spec = GetSingleSpectrum(data, param1index=i, band=band)
        p1 = '%.3E' %param1values[i]
        outfilename = param1name + p1
        if band:
            SingleSpectrum(data, spec, outfile=outfilename, xscale=xscale, yscale=yscale, band='narrow',
                           fScaling=fScaling, xlabelUnit=xlabelUnit)
        else:
            SingleSpectrum(data, spec, outfile=outfilename, xscale=xscale, yscale=yscale, fScaling=fScaling,
                           xlabelUnit=xlabelUnit)


def AllSpectra1Param2D(data, param1index, param2values, param1name, param2name,
                        xscale='lin', yscale='lin', band=False, fScaling=1e-9, xlabelUnit="GHz"):
    """ In a batch job where two geometry parameters are varied, for a specified index of the first parameter,
        individually plot all spectra for every value the second parameter.
        data:         wgbpm.Data.Data2D object containing the 2D array of spectra
        param1index:  index of the first varied parameter
        param1name:   name and value of the first varied parameter, e.g "WaveguideWidth_0.01m". Is used in the
                      output file name.
        param2values: all values of the second varied parameter
        param2name:   name of the second varied parameter, e.g "WaveguideHeight". Is used in the output file, is
                      appended with appropriate value from param2values for each plot.
        xscale:       'lin' or 'log'
        yscale:       'lin' or 'log'
        band:         optionally plot the band spectrum rather than the full spectrum, default is False
        fScaling:     frequency scaling factor. Default=1e-9 (Hz to GHz)
        xlabelUnit:   frequency unit in the xlabel. Default="GHz"
        """
    CheckDataIs2D(data)

    for i in range(len(param2values)):
        spec = GetSingleSpectrum(data, param1index=param1index, param2index=i, band=band)
        p2 = '%.3E' %param2values[i]
        outfilename = param1name + "_" + param2name + p2
        if band:
            SingleSpectrum(data, spec, outfile=outfilename, xscale=xscale, yscale=yscale, band='narrow',
                           fScaling=fScaling, xlabelUnit=xlabelUnit)
        else:
            SingleSpectrum(data, spec, outfile=outfilename, xscale=xscale, yscale=yscale, fScaling=fScaling,
                           xlabelUnit=xlabelUnit)


def AllSpectra2Params(data, param1values, param2values, param1name, param2name,
                      xscale='lin', yscale='lin', band=False, fScaling=1e-9, xlabelUnit="GHz"):
    """ In a batch job where two geometry parameters are varied, individually plot all spectra for every value
        of both parameters.
        data:         wgbpm.Data.Data2D object containing array of spectra
        param1values: all values of the first varied parameter
        param2values: all values of the second varied parameter
        param1name:   name of the first varied parameter, e.g "WaveguideWidth". Is used in the output file, is
                      appended with appropriate value from param1values for each plot.
        param2name:   name of the second varied parameter, e.g "WaveguideHeight". Is used in the output file, is
                      appended with appropriate value from param2values for each plot.
        xscale:       'lin' or 'log'
        yscale:       'lin' or 'log'
        band:         optionally plot the band spectrum rather than the full spectrum, default is False
        fScaling:     frequency scaling factor. Default=1e-9 (Hz to GHz)
        xlabelUnit:   frequency unit in the xlabel. Default="GHz"
        """
    CheckDataIs2D(data)

    for i in range(len(param1values)):
        for j in range(len(param2values)):
            spec = GetSingleSpectrum(data, param1index=i, param2index=j, band=band)
            p1 = '%.3E' %param1values[i]
            p2 = '%.3E' %param2values[j]

            outfilename = param1name + p1 + "_" + param2name + p2
            if band:
                SingleSpectrum(data, spec, outfile=outfilename, xscale=xscale, yscale=yscale, band='narrow',
                               fScaling=fScaling, xlabelUnit=xlabelUnit)
            else:
                SingleSpectrum(data, spec, outfile=outfilename, xscale=xscale, yscale=yscale,
                               fScaling=fScaling, xlabelUnit=xlabelUnit)


def AllBandSpectra1Param1D(data, param1values, param1name, xscale='lin', yscale='lin', fScaling=1e-9, xlabelUnit="GHz"):
    """ In a batch job where one geometry parameter is varied, individually plot all band spectra for every value
        of that parameter.
        data:         wgbpm.Data.Data1D object containing array of spectra
        param1values: all values of the varied parameter
        param1name:   name of the varied parameter, e.g "WaveguideWidth". Is used in the output file, is
                      appended with appropriate value from param1values for each plot.
        xscale:       'lin' or 'log'
        yscale:       'lin' or 'log'
        fScaling:     frequency scaling factor. Default=1e-9 (Hz to GHz)
        xlabelUnit:   frequency unit in the xlabel. Default="GHz"
        """
    AllSpectra1Param1D(data, param1values, param1name, xscale, yscale, band=True, fScaling=fScaling, xlabelUnit=xlabelUnit)


def AllBandSpectra1Param2D(data, param1index, param2values, param1name, param2name,
                           xscale='lin', yscale='lin', fScaling=1e-9, xlabelUnit="GHz"):
    """ In a batch job where two geometry parameters are varied, for a specified index of the first parameter,
        individually plot all band spectra for every value the second parameter.
        data:         wgbpm.Data.Data2D object containing the 2D array of spectra
        param1index:  index of the first varied parameter
        param1name:   name and value of the first varied parameter, e.g "WaveguideWidth_0.01m". Is used in the
                      output file name.
        param2values: all values of the second varied parameter
        param2name:   name of the second varied parameter, e.g "WaveguideHeight". Is used in the output file, is
                      appended with appropriate value from param2values for each plot.
        xscale:       'lin' or 'log'
        yscale:       'lin' or 'log'
        fScaling:     frequency scaling factor. Default=1e-9 (Hz to GHz)
        xlabelUnit:   frequency unit in the xlabel. Default="GHz"
        """
    AllSpectra1Param2D(data, param1index, param2values, param1name, param2name, xscale, yscale, band=True,
                       fScaling=fScaling, xlabelUnit=xlabelUnit)


def AllBandSpectra2Params(data, param1values, param2values, param1name, param2name,
                          xscale='lin', yscale='lin', fScaling=1e-9, xlabelUnit="GHz"):
    """ In a batch job where two geometry parameters are varied, individually plot all band spectra for every value
        of both parameters.
        data:         wgbpm.Data.Data2D object containing array of spectra
        param1values: all values of the first varied parameter
        param2values: all values of the second varied parameter
        param1name:   name of the first varied parameter, e.g "WaveguideWidth". Is used in the output file, is
                      appended with appropriate value from param1values for each plot.
        param2name:   name of the second varied parameter, e.g "WaveguideHeight". Is used in the output file, is
                      appended with appropriate value from param2values for each plot.
        xscale:       'lin' or 'log'
        yscale:       'lin' or 'log'
        fScaling:     frequency scaling factor. Default=1e-9 (Hz to GHz)
        xlabelUnit:   frequency unit in the xlabel. Default="GHz"
        """
    AllSpectra2Params(data, param1values, param2values, param1name, param2name, xscale, yscale, band=True,
                      fScaling=fScaling, xlabelUnit=xlabelUnit)


def _SpecVsParameter(data, paramvalues, paramindex=None, outfile='', xscale='lin', yscale='lin', ylabel="",
                     lowerfreq=5e9, upperfreq=50e9, band='full', targetfreq=15e9, scaling=1000, fScaling=1e-9,
                     xlabelUnit="GHz"):
    if band == 'full':
        freq = data.freq
        spec = GetTwoDSpectrum(data, param1index=paramindex)
        banddir = 'full/'
    elif band == 'wide':
        spec = GetTwoDSpectrum(data, param1index=paramindex)
        freq, spec = Cut2DFreqData(data, lowerfreq, upperfreq, spec)
        banddir = 'band/'
    elif band == 'narrow':
        freq = data.bandfreq
        spec = GetTwoDSpectrum(data, param1index=paramindex, band=True)
        banddir = 'band/'
    else:
        raise ValueError("Unknown band: " + band + ". Only bands allowed are 'full', 'wide', and 'narrow' ")

    ydat = paramvalues * scaling  # y unit in mm

    xscal, yscal = GetFigureScales(xscale, yscale)

    targetx, targety = GetTargetFreqArrays(ydat, targetfreq*fScaling)  # the 15 GHz target line

    fig, ax = plt.subplots()
    lev_exp = np.arange(np.floor(np.log10(data.minspec)-1), np.ceil(np.log10(data.maxspec)+1), 0.2)
    levs = np.power(10, lev_exp)
    cs = ax.contourf(freq*fScaling, ydat, spec, levs, norm=LogNorm())
    ax.plot(targetx, targety, color='k', linestyle='--', alpha=0.5)
    ax.set_xscale(xscal)
    ax.set_yscale(yscal)
    ax.set_ylim((min(ydat), max(ydat)))
    ax.set_xlabel("Freq "+xlabelUnit)
    ax.set_ylabel(ylabel)
    cbar = fig.colorbar(cs)
    cbar.set_label('Sqrt Power / Hz (sqrt(W) / Hz)')
    plt.show()

    filename = 'SpecVsParam_'
    if band != "full":
        filename = 'BandSpecVsParam_'

    SaveFreqFig(banddir+filename+outfile, xscale=xscale, yscale=yscale)
    plt.close('all')


def SpectrumVsParameter(data, paramvalues, param1index=None, outfile='', xscale='lin', yscale='lin', ylabel="",
                        targetfreq=15e9, scaling=1000, fScaling=1e-9, xlabelUnit="GHz"):
    """ Generate a 2D plot of all full spectra against a varied geometry parameter
        data:        wgbpm.Data.Data1D or wgbpm.Data.Data2D object
        paramvalues: all values of the varied geometry parameter, will be the plots y-axis
        param1index: index of second varied geometry parameter if data is a Data2D instance
        outfile:     string for the output file name
        xscale:      'lin' or 'log'
        yscale:      'lin' or 'log'
        ylabel:      label for plot y-axis, e.g. Waveguide Width (mm)
        targetfreq:  optionally draw a dashed line at a target frequency (Hz). Set to 0 if no line wanted.
                     Must be unscaled - fScaling factor is applied to targetfreq within this function
        scaling:     scaling factor for the y-axis values. Default=1000
        fScaling:    frequency scaling factor. Default=1e-9 (Hz to GHz).
        xlabelUnit:  frequency unit in the xlabel. Default="GHz"
        """
    _SpecVsParameter(data, paramvalues, param1index, outfile, xscale, yscale, ylabel, band='full',
                     targetfreq=targetfreq, scaling=scaling, fScaling=fScaling, xlabelUnit=xlabelUnit)


def SpectrumBandVsParameter(data, paramvalues, param1index=None, outfile='', xscale='lin', yscale='lin', ylabel='',
                            targetfreq=15e9, scaling=1000, fScaling=1e-9, xlabelUnit="GHz"):
    """ Generate a 2D plot of all band spectra against a varied geometry parameter
        data:        wgbpm.Data.Data1D or wgbpm.Data.Data2D object
        paramvalues: all values of the varied geometry parameter, will be the plots y-axis
        param1index: index of second varied geometry parameter if data is a Data2D instance
        outfile:     string for the output file name
        xscale:      'lin' or 'log'
        yscale:      'lin' or 'log'
        ylabel:      label for plot y-axis, e.g. Waveguide Width (mm)
        targetfreq:  optionally draw a dashed line at a target frequency (Hz). Set to 0 if no line wanted.
                     Must be unscaled - fScaling factor is applied to targetfreq within this function
        scaling:     scaling factor for the y-axis values. Default=1000
        fScaling:    frequency scaling factor. Default=1e-9 (Hz to GHz).
        xlabelUnit:  frequency unit in the xlabel. Default="GHz"
        """
    _SpecVsParameter(data, paramvalues, param1index, outfile, xscale, yscale, ylabel,
                     band='narrow', targetfreq=targetfreq, scaling=scaling, fScaling=fScaling, xlabelUnit=xlabelUnit)


def SpectrumBandUserVsParameter(data, paramvalues, param1index=None, outfile='', xscale='lin', yscale='lin', ylabel='',
                        lowerfreq=10e9, upperfreq=20e9, targetfreq=15e9, scaling=1000, fScaling=1e-9, xlabelUnit="GHz"):
    """ Generate a 2D plot of all spectra in user defined band against a varied geometry parameter. Designed to narrow
        down band window if band spectra in data instance is too wide.
        data:        wgbpm.Data.Data1D or wgbpm.Data.Data2D object
        paramvalues: all values of the varied geometry parameter, will be the plots y-axis
        param1index: index of second varied geometry parameter if data is a Data2D instance
        outfile:     string for the output file name
        xscale:      'lin' or 'log'
        yscale:      'lin' or 'log'
        ylabel:      label for plot y-axis, e.g. Waveguide Width (mm)
        lowerfreq:   lower frequency limit (Hz) in "wide" band
        upperfreq:   upper frequency limit (Hz) in "wide" band
        targetfreq:  optionally draw a dashed line at a target frequency (Hz). Set to 0 if no line wanted.
                     Must be unscaled - fScaling factor is applied to targetfreq within this function
        scaling:     scaling factor for the y-axis values. Default=1000
        fScaling:    frequency scaling factor. Default=1e-9 (Hz to GHz).
        xlabelUnit:  frequency unit in the xlabel. Default="GHz"
        """
    _SpecVsParameter(data, paramvalues, param1index, outfile, xscale, yscale, ylabel, lowerfreq=lowerfreq,
                     upperfreq=upperfreq, band='wide', targetfreq=targetfreq, scaling=scaling, fScaling=fScaling,
                     xlabelUnit=xlabelUnit)


def SpectrumBandVsParameterAndSample(data, paramvalues, param1index=None, outfile='', ylabel="", parameter="",
                                     lowerfreq=5e9, upperfreq=50e9, band="narrow", targetfreq=15e9,
                                     sampleindices=[0], scaling=1000, fScaling=1e-9, xlabelUnit="GHz"):
    """ Generate a 2D plot of a spectra band against a varied geometry parameter with a subplot containing
        example spectra
        data:        wgbpm.Data.Data1D or wgbpm.Data.Data2D object
        paramvalues: all values of the varied geometry parameter, will be the plots y-axis
        param1index: index of second varied geometry parameter if data is a Data2D instance
        outfile:     string for the output file name
        ylabel:      label for plot y-axis, e.g. Waveguide Width (mm)
        parameter:   string used in label of samples e.g. "Waveguide Width".
        lowerfreq:   lower frequency limit (Hz). Must be unscaled.
        upperfreq:   upper frequency limit (Hz). Must be unscaled.
        band:        the spectrum range plotted, either "wide" between the specified lowerfreq and upperfreq
                     from the full spectrum, or "narrow" for the Data1D or Data2D band spectrum.
        targetfreq:  optionally draw a dashed line at a target frequency (Hz). Set to 0 if no line wanted.
                     Must be unscaled - fScaling factor is applied to targetfreq within this function
        sampleindices: iterable containing indices of spectra that will be plotted in subplot
        scaling:     scaling factor for the y-axis values. Default=1000
        fScaling:    frequency scaling factor. Default=1e-9 (Hz to GHz).
        xlabelUnit:  frequency unit in the xlabel. Default="GHz"
        """
    if band == 'wide':
        spec = GetTwoDSpectrum(data, param1index=param1index)
        freq, spec = Cut2DFreqData(data, lowerfreq, upperfreq, spec)
    elif band == 'narrow':
        freq = data.bandfreq
        spec = GetTwoDSpectrum(data, param1index=param1index, band=True)
    else:
        raise ValueError("Unknown band: " + band + ". Only bands allowed are 'full', 'wide', and 'narrow' ")

    fig = plt.figure(figsize=(8,10))
    ax0 = plt.subplot2grid((3, 1), (0, 0), rowspan=1)
    ax1 = plt.subplot2grid((3, 1), (1, 0), rowspan=2)

    targetx, targety = GetTargetFreqArrays(data, targetfreq*fScaling)  # the 15 GHz target line
    target_line_y2d = np.linspace(min(paramvalues), max(paramvalues), 10)  # line for contour plot

    lev_exp = np.arange(np.floor(np.log10(data.minspec)-1), np.ceil(np.log10(data.maxspec)+1), 0.5)
    levs = np.power(10, lev_exp)
    cs = ax1.contourf(freq*fScaling, np.array(paramvalues)*scaling, spec, levs, norm=LogNorm())
    ax1.plot(targetx, target_line_y2d * scaling, color='k', linestyle='--', alpha=0.5)
    ax1.set_xlabel("Freq "+xlabelUnit)
    ax1.set_ylabel(ylabel)
    cbar = fig.colorbar(cs)
    cbar.set_label('Sqrt Power / Hz (sqrt(W) / Hz)')
    ax1.set_position([0.1552, 0.11, 0.62, 0.5])

    for i in sampleindices:
        ax0.plot(freq*fScaling, np.array(spec[i]), color='b', label=parameter + ' = ' + str(paramvalues[i]))
    ax0.plot(targetx, targety * scaling, color='k', linestyle='--', alpha=0.5)
    ax0.set_ylabel('Sqrt Power / Hz (sqrt(W) / Hz)')
    ax0.set_xlim(10, 20)
    ax0.set_ylim(data.minspec, data.maxspec)
    ax0.set_position([0.1552, 0.63, 0.62, 0.3])
    ax0.get_xaxis().set_visible(False)
    ax0.legend(loc=4)
    plt.show()
    SaveFreqFigScaled('band/', 'BandSpecVsParamAndSamp_'+outfile, 'lin', 'lin')
    plt.close('all')


####################################################################

def SingleSignal(data, signal=None, param1index=None, param2index=None, outfile='', maxtime=2.5e-9, tscaling=1e9,
                 xlabelUnit="ns", ylabelUnit="V"):
    """ Plot a single time domain signal generated by Gdfidl.
        data:        wgbpm.Data.Data1D or wgbpm.Data.Data2D object
        signal:      1D array of voltage signal, taken from data.twod_volts if not supplied
        param1index: if signal is not supplied, param1index is the index of varied geometry parameter in Data1D
                     instance or index of first varied geometry parameter in Data2D instance
        param2index: index of second varied geometry parameter in Data2D instance if signal is not supplied
        outfile:     string for the output file name
        paramstring: string representing the parameter varied and value, e.g "WaveguideWidth_0.01m", used in plot
                     file name.
        uppertime:   max time (s)
        tscaling:    scaling factor on x-axis (time). Default = 1e9 (i.e. s to ns)
        xlabelUnit:  time unit in the xlabel. Default="ns"
        ylabelUnit:  unit in the ylabel. Default="V"
        """

    signal = GetSingleSignal(data, signal, param1index, param2index)

    cuttime, cutdata = Cut1DTimeData(data, maxtime, signal)
    lims = max(data.maxvolt, abs(data.minvolt))
    plt.figure()
    plt.plot(cuttime*tscaling, cutdata)
    plt.xlabel("Time "+xlabelUnit)
    plt.ylabel("Signal Voltage "+ylabelUnit)
    plt.ylim(-lims, lims)
    plt.tight_layout()
    SaveTimeFig(outfile)
    plt.close()


def AllSignals1Param1D(data, param1values, param1name, tscaling=1e9, xlabelUnit="ns", ylabelUnit="V"):
    """ In a batch job where one geometry parameter is varied, individually plot the time domain signals for
        every value of that parameter.
        data:         wgbpm.Data.Data1D object containing array of spectra
        param1values: all values of the varied parameter
        param1name:   name of the varied parameter, e.g "WaveguideWidth". Is used in the output file, is
                      appended with appropriate value from param1values for each plot.
        tscaling:     scaling factor on x-axis (time). Default = 1e9 (i.e. s to ns)
        xlabelUnit:   time unit in the xlabel. Default="ns"
        ylabelUnit:   unit in the ylabel. Default="V"
        """
    CheckDataIs1D(data)

    for i in range(len(param1values)):
        signal = GetSingleSignal(data, param1index=i)
        p1 = '%.3E' %param1values[i]
        outfilename = param1name + p1
        SingleSignal(data, signal, outfile=outfilename, tscaling=tscaling, xlabelUnit=xlabelUnit, ylabelUnit=ylabelUnit)


def AllSignals1Param2D(data, param1index, param2values, param1name, param2name, tscaling=1e9,
                       xlabelUnit="ns", ylabelUnit="V"):
    """ In a batch job where two geometry parameters are varied, for a specified index of the first parameter,
        individually plot the time domain signals for every value the second parameter.
        data:         wgbpm.Data.Data2D object containing the 2D array of spectra
        param1index:  index of the first varied parameter
        param1name:   name and value of the first varied parameter, e.g "WaveguideWidth_0.01m". Is used in the
                      output file name.
        param2values: all values of the second varied parameter
        param2name:   name of the second varied parameter, e.g "WaveguideHeight". Is used in the output file, is
                      appended with appropriate value from param2values for each plot.
        tscaling:     scaling factor on x-axis (time). Default = 1e9 (i.e. s to ns)
        xlabelUnit:   time unit in the xlabel. Default="ns"
        ylabelUnit:   unit in the ylabel. Default="V"
        """
    CheckDataIs2D(data)

    for i in range(len(param2values)):
        signal = GetSingleSignal(data, param1index=param1index, param2index=i)
        p2 = '%.3E' %param2values[i]
        outfilename = param1name + "_" + param2name + p2
        SingleSignal(data, signal, outfile=outfilename, tscaling=tscaling, xlabelUnit=xlabelUnit, ylabelUnit=ylabelUnit)


def AllSignals2Params(data, param1values, param2values, param1name, param2name, tscaling=1e9,
                      xlabelUnit="ns", ylabelUnit="V"):
    """ In a batch job where two geometry parameters are varied, individually plot the time domain signals
        for every value of both parameters.
        data:         wgbpm.Data.Data2D object containing array of spectra
        param1values: all values of the first varied parameter
        param2values: all values of the second varied parameter
        param1name:   name of the first varied parameter, e.g "WaveguideWidth". Is used in the output file, is
                      appended with appropriate value from param1values for each plot.
        param2name:   name of the second varied parameter, e.g "WaveguideHeight". Is used in the output file, is
                      appended with appropriate value from param2values for each plot.
        tscaling:     scaling factor on x-axis (time). Default = 1e9 (i.e. s to ns)
        xlabelUnit:   time unit in the xlabel. Default="ns"
        ylabelUnit:   unit in the ylabel. Default="V"
        """
    CheckDataIs2D(data)

    for i in range(len(param1values)):
        for j in range(len(param2values)):
            signal = GetSingleSignal(data, param1index=i, param2index=j)
            p1 = '%.3E' %param1values[i]
            p2 = '%.3E' %param2values[j]
            outfilename = param1name + p1 + "_" + param2name + p2
            SingleSignal(data, signal, outfile=outfilename, tscaling=tscaling, xlabelUnit=xlabelUnit, ylabelUnit=ylabelUnit)


def SignalGrid(data, variedparamname, variedparamvalues, param1index=None, param1name=None, maxtime=2.5e-9,
               subfigsperplot=9, figsize=(9,9), tscaling=1e9, yscaling=1000, xlabelUnit="ns", ylabelUnit="V"):
    """ In a batch job where one or two geometry parameters are varied, plot a grid of subfigures each containing
        a time domain signal. If only one parameter is varied, simply plot the signals. If two parameters are varied,
        an index for a specific value of the first parameter must be supplied, with all signals for the second varied
        parameter being plotted.
        data:              wgbpm.Data.Data1D or wgbpm.Data.Data2D instance
        variedparamname:   used in the output file name, should be the name of the varied geometry parameter,
                           e.g "WaveguideWidth".
        variedparamvalues: all values of the varied geometry parameter
        param1index:       index of the first varied geometry parameter if data is a Data2D instance
        param1name:        used in the output file name if data is a Data2D instance. Should be the name and value of
                           the first varied geometry parameter, e.g. "WaveguideHeight_0.01m"
        uppertime:         max time (s)
        subfigsperplot:    number of subfigures per plot
        figsize:           figure size
        tscaling:          scaling factor on x-axis (time). Default = 1e9 (i.e. s to ns)
        yscaling:          scaling factor on the y-axis. Default = 1000
        xlabelUnit:        time unit in the xlabel. Default="ns"
        ylabelUnit:        unit in the ylabel. Default="V"
        """

    if isinstance(data, Data.Data1D):
        signal = data.twod_volts
        outfilename = 'SignalGrid_' + variedparamname
    elif isinstance(data, Data.Data2D):
        if param1index is None:
            raise ValueError("data is a Data2D instance, an index for a value of the first varied "
                             "geometry parameter must be supplied")
        if param1name is None:
            raise ValueError("data is a Data2D instance, a name the first varied geometry parameter must be supplied")
        signal = data.twod_volts[param1index]
        outfilename = 'SignalGrid_' + param1name + "_" + variedparamname
    else:
        raise TypeError("data is not a wgbpm.Data.Data1D or wgbpm.Data.Data2D instance")

    cuttime, cutdata = Cut2DTimeData(data, maxtime, signal)

    lims = max(data.maxvolt, abs(data.minvolt))
    numrnc = int(np.ceil(np.sqrt(subfigsperplot)))  # number of rows and columns

    remainder, numfigs = GetNumFigsAndRemainder(variedparamvalues, subfigsperplot)

    for i in range(numfigs):
        datatoplot = cutdata[i*subfigsperplot: i*subfigsperplot + subfigsperplot, :]
        fig1, axs = plt.subplots(numrnc, numrnc, figsize=figsize)
        for col in range(1, numrnc):
            for row in range(0, numrnc-1):
                axs[row, col].xaxis.set_visible(False)
            for row in range(0, numrnc):
                axs[row, col].yaxis.set_visible(False)
        for row in range(0, numrnc - 1):
            for col in range(0, numrnc):
                axs[row, col].xaxis.set_visible(False)
        axs = trim_axs(axs, subfigsperplot)
        indexes = range(len(axs))
        for index, ax in zip(indexes, axs):
            if (i == numfigs - 1) and (index >= remainder):
                ax = HideAxis(ax)
            else:
                leg = variedparamname + '= %.4f' % (yscaling*variedparamvalues[i*subfigsperplot + index])
                ax.plot(cuttime*tscaling, datatoplot[index], 'x', ls='-', ms=2, label=leg)
                ax.set_ylim(-lims, lims)
                ax.legend(fontsize=8)
                ax.set_xlabel("Time "+xlabelUnit)
                ax.set_ylabel("Signal Voltage "+ylabelUnit)
        plt.tight_layout()
        SaveTimeFig(outfilename + str(i))
        plt.close('all')


def SignalVsParameter(data, paramvalues, param1index=None, outfile='', ylabel="", maxtime=2.5e-9,
                      tscaling=1e9, yscaling=1000, xlabelUnit="ns", ylabelUnit="V"):
    """ Generate a 2D plot of all time domain signals against a varied geometry parameter
        data:        wgbpm.Data.Data1D or wgbpm.Data.Data2D object
        paramvalues: all values of the varied geometry parameter, will be the plots y-axis
        param1index: index of second varied geometry parameter if data is a Data2D instance
        outfile:     string for the output file name
        ylabel:      label for plot y-axis, e.g. Waveguide Width (mm)
        maxtime:     max time (s)
        tscaling:    scaling factor on x-axis (time). Default = 1e9 (i.e. s to ns)
        yscaling:    scaling factor on the y-axis. Default = 1000
        xlabelUnit:  time unit in the xlabel. Default="ns"
        ylabelUnit:  unit in the ylabel. Default="V"
        """
    if isinstance(data, Data.Data2D):
        if param1index is None:
            raise ValueError("no index supplied for selecting 2D signal in Data2D instance")
        signal = data.twod_volts[param1index]
    elif isinstance(data, Data.Data1D):
        signal = data.twod_volts
    else:
        raise TypeError("data must be a wgbpm.Data.Data1D or wgbpm.Data.Data2D instance")

    cuttime, cutdata = Cut2DTimeData(data, maxtime, signal)

    fig, ax = plt.subplots()
    deltav = (data.maxvolt - data.minvolt) / 50
    lev = np.arange(data.minvolt, data.maxvolt, deltav)
    cs = ax.contourf(cuttime*tscaling, np.array(paramvalues)*yscaling, cutdata, lev)
    ax.set_xlabel("Time "+xlabelUnit)
    ax.set_ylabel(ylabel)
    cbar = fig.colorbar(cs)
    cbar.set_label("Signal Voltage "+ylabelUnit)
    plt.show()
    SaveTimeFig('SignalVsParam_'+outfile)
    plt.close('all')


