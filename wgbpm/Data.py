import pickle as _pick
import numpy as _np
import pandas as _pan
from .General import *


class Batch(dict):
    """
    Data container for information about the directory structure for a batch of jobs. Class attributes:
    self.localdir:    full path of the local directory where the users script is and where it is run from.
                      E.g. /home/wshields/wgbpm/local/wgBpm/
    self.resdir:      full path of the directory where all results are stored for many batches
                      E.g. /home/wshields/wgbpm/local/wgBpm/results/
    self.batchresdir: full path of the directory this batch's results & plots will be stored. Should be a unique path.
                      E.g. /home/wshields/wgbpm/local/wgBpm/results/20200601_163218
    self.datadir:     full path of the directory where all data is stored for many batches
                      E.g. /home/wshields/wgbpm/data/
    self.batchdatadir: full path of the directory this batch's data will be stored. Should be a unique path.
                      E.g. /home/wshields/wgbpm/data/20200601_163218
    """
    def __init__(self):
        super(Batch, self).__init__()
        self.localdir = ""
        self.resdir = ""
        self.batchresdir = ""
        self.datadir = ""
        self.batchdatadir = ""
        self.fdomainDataFile = 'scratch.SigOut_out_1-freq-abs.mtv'
        self.tdomainDataFile = 'scratch.Port=SigOut-e_amp_of_mode=0001-time.mtv'
        self.joblist = []

    def __repr__(self):
        s = "wgbpm.Data.Batch instance\n"
        vars = ["name", "localdir", "datadir", "resdir", "batchresdir", "batchdatadir","fdomainDataFile","tdomainDataFile"]
        for v in vars:
            val = getattr(self, v)
            if val != "":
                s += v + str(val) + "\n"
        print(s)

    def _setvar(self, attr, value):
        if not isinstance(value, str):
            raise TypeError("unknown data type for " + attr + ", value must be a string")
        setattr(self, attr, value)

    def SetLocalDir(self, path):
        self._setvar("localdir", path)

    def SetDataDir(self, path):
        self._setvar("datadir", path)

    def SetResDir(self, path):
        self._setvar("resdir", path)

    def SetBatchName(self, batchname):
        self._setvar("batchresdir", self.resdir + batchname)
        self._setvar("batchdatadir", self.datadir + batchname)

    def SetFreqDomainDataFile(self, datafile):
        self._setvar("fdomainDataFile", datafile)

    def SetTimeDomainDataFile(self, datafile):
        self._setvar('tdomainDataFile', datafile)

    def AddJob(self, job):
        self.joblist.append(job)


class Job(object):
    """ Data container for an individual job. Contains job & simulation settings as well as  GdfidL output data. """
    def __init__(self, geom, batchinfo, jobname,
                 nodes=1,
                 taskspernode=1,
                 cpuspertask=1,
                 tasktime="01:00:00",
                 includeNodes=None,
                 excludeNodes=None):
        """ geom:         wgbpm.Data.Geom instance containing the model's geometry parameters
            batchinfo:    wgbpm.Data.Batch instance containing the batch job info that this job is a part of
            jobname:      the job's name - used as job submission name & directory name for running & output storage.
                          Must be unique in a batch of jobs. E.g slw_0.016_slh_0.002
            nodes:        the number of nodes each job will use. Default = 1
            taskspernode: the number of tasks each job will run on a node. Default = 1
            cpuspertask:  the number of cpus each task will use. Default = 1
            tasktime:     the task time after which the job will be killed. Should be in the format "hh:mm:ss" Default=01:00:00
            includeNodes: Specify the nodes that will be used in this job. Must be a string or iterable container
                          of strings,  e.g "compute001" or "[compute001, compute002]. Default is None, i.e nodes randomly selected.
            excludeNodes: Specify the nodes that will be excluded from use in this job. Must be a string or iterable container
                          of strings,  e.g "compute001" or "[compute001, compute002]. Default is None, i.e every node can be used.
            """

        if not isinstance(geom, Geom):
            raise TypeError("job geometry must be a wgbpm.Data.Geom instance")
        self.geom = geom
        self.batchinfo = batchinfo
        self.jobname = jobname
        self.nodes = nodes
        self.taskspernode = taskspernode
        self.cpuspertask = cpuspertask
        self.nthreads = nodes * taskspernode * cpuspertask
        self.tasktime = tasktime
        self.includeNodes = includeNodes
        self.excludeNodes = excludeNodes
        self.datapath = ""

    def AddFreq(self, freq):
        setattr(self, 'freq', freq)

    def AddSpectrum(self, spec):
        setattr(self, 'spectrum', spec)

    def SetDataPath(self, path):
        """ Set unique temporary path where the GdfidL model will be stored, run, and output saved.
            E.g. /home/wshields/wgbpm/local/wgBpm/slw_0.016_slh_0.002/"""
        self.datapath = path

    def AddTime(self, time):
        setattr(self, 'time', time)

    def AddVoltage(self, voltage):
        setattr(self, 'voltage', voltage)


class Geom(dict):
    def __init__(self, **kwargs):
        """ Simple data container for holding the geometry values for a single job."""
        super(Geom, self).__init__()
        for key in kwargs:
            self[key] = str(kwargs.get(key))
            setattr(self, key, str(kwargs.get(key)))

    def SetParam(self, param, value):
        self[param] = value

    def __repr__(self):
        s = ""
        for i in self.keys():
            if len(self[i]) > 1:
                data = self[i]
                stepstr = GetSpacingString(data)
                t = i + ": From " + str(min(self[i])) + " to " + str(max(self[i])) + ", step = " + stepstr
                s += t + "\n"
            else:
                s += i + ": " + str(self[i][0]) + "\n"
        return s


class DataBase(object):
    def __init__(self, batchjob):
        """ Data container base class.
            batchjob: wgbpm.Data.Batch instance containing necessary info for the batch job
            """
        files = GetPickledData(batchjob)
        data = [_pick.load(open(file, "rb")) for file in files]
        keys = data[0].geom.keys()
        self._varied = []  # hidden variable of names of parameters that are varied
        self.Geom = Geom()
        for key in keys:
            try:
                vals = [float(i.geom[key]) for i in data]  # get data values for every param in the geom class
            except ValueError:
                try:
                    vals = [float(i.geom[key][0]) for i in data]
                except (ValueError, IndexError):
                    print("Cannot get value(s) for "+key+", setting to zero")
                    vals = [0 for i in data]
            setattr(self, key, _np.unique(vals))       # set class members to be the unique values of the geometry
            self.Geom.SetParam(key, _np.unique(vals))  # set Geom members to be unique values of the geometry
            if _np.std(getattr(self, key)) > 0:
                self._varied.append(key)
        self._data = data
        self.freqs = None
        self.specs = None
        self.times = None
        self.volts = None

    def Varied(self):
        """ Print list of the varied geometry parameters, with their min, max, and step values."""
        if len(self._varied) > 0:
            s = ""
            for i in self._varied:
                data = self.Geom[i]
                stepstr = GetSpacingString(data)
                s += i + ": From " + str(min(data)) + " to " + str(max(data)) + ", step = " + stepstr + "\n"
            print(s)
        else:
            print("No varied geometry parameters found")

    def GetLimitsAndLens1D(self, p1):
        """ For a batch job where two geometry parameters are varied, get the lengths and the max and min limits
            of the spectral (power & frequency) and signal (voltage and time) data generated for all varied values.
            Returns list of tuples (each containing lists of the min and max values) for "specs", "freqs", "volts",
            and "times", in that order. The final tuple is two lists of the lengths of the "freqs" and "times" data
            in that order.
            """
        params = ["specs", "freqs", "volts", "times"]
        lims = []
        lenf = []
        lent = []
        for param in params:
            maxvals = []
            minvals = []
            for i in range(len(p1)):
                data = getattr(self, param)[i, 0]
                if data is not None:
                    maxvals.append(max(data))
                    if param == "specs" and (min(data) > 0):
                        minvals.append(min(data))
                    elif param != "specs":
                        minvals.append(min(data))
                    if param == "freqs":
                        lenf.append(len(data))
                    if param == "times":
                        lent.append(len(data))
            # check if min and max for spectra exist, warn and set to zero if no min/max found.
            if param == "specs" and len(minvals) == 0:
                print("warning: no min/max detected in spectra - something may have gone wrong with the simulation")
                minvals.append(0)
                maxvals.append(0)
            lims.append((minvals, maxvals))
        lims.append((lenf, lent))
        return lims

    def GetLimitsAndLens2D(self, p1, p2):
        """ For a batch job where a geometry parameter is varied, get the lengths and the max and min limits of the
            spectral (power & frequency) and signal (voltage and time) data generated for all varied values.
            Returns list of tuples (each containing lists of the min and max values) for "specs", "freqs", "volts",
            and "times", in that order. The final tuple is two lists of the lengths of the "freqs" and "times" data
            in that order.
            """
        params = ["specs", "freqs", "volts", "times"]
        lims = []
        lenf = []
        lent = []
        for param in params:
            maxvals = []
            minvals = []
            for i in range(len(p1)):
                for j in range(len(p2)):
                    data = getattr(self, param)[i, j]
                    if data is not None:
                        maxvals.append(max(data))
                        if param == "specs" and (min(data) > 0):
                            minvals.append(min(data))
                        elif param != "specs":
                            minvals.append(min(data))
                        if param == "freqs":
                            lenf.append(len(data))
                        if param == "times":
                            lent.append(len(data))
            # check if min and max for spectra exist, warn and set to zero if no min/max found.
            if param == "specs" and len(minvals) == 0:
                print("warning: no min/max detected in spectra - something may have gone wrong with the simulation")
                minvals.append(0)
                maxvals.append(0)
            lims.append((minvals, maxvals))
        lims.append((lenf, lent))
        return lims


class Data1D(DataBase):
    def __init__(self, batchjob, param1, bandlow=10e9, bandhigh=20e9):
        """ Data container for a batch job where one geometry parameter is varied.
            batchjob: wgbpm.Data.Batch instance containing necessary info for the batch job
            param1: name of parameter varied, e.g. SlW
            bandlow: lower frequency bound to select narrower region of interest (Hz)
            bandhigh: higher frequency bound to select narrower region of interest (Hz)
            """
        super(Data1D, self).__init__(batchjob)
        p1 = getattr(self, param1)
        # empty arrays for signal & spectral data
        self.freqs = _np.empty((len(p1), 1), dtype=_pan.core.series.Series)
        self.specs = _np.empty((len(p1), 1), dtype=_pan.core.series.Series)
        self.times = _np.empty((len(p1), 1), dtype=_pan.core.series.Series)
        self.volts = _np.empty((len(p1), 1), dtype=_pan.core.series.Series)

        self.geoms = _np.empty((len(p1), 1), dtype=dict) # empty array to contain geometry instances for each job

        for i in self._data:
            p1ind = list(p1).index(float(i.geom[param1]))
            self.freqs[p1ind, 0] = i.freq
            self.specs[p1ind, 0] = i.spectrum
            self.times[p1ind, 0] = i.time
            self.volts[p1ind, 0] = i.voltage
            self.geoms[p1ind, 0] = i.geom

        lensandlims = self.GetLimitsAndLens1D(p1)  # order: spec, freq, volts, times, and lens (freqs, times)

        self.minspec = min(lensandlims[0][0])
        self.maxspec = max(lensandlims[0][1])
        self.minvolt = min(lensandlims[2][0])
        self.maxvolt = max(lensandlims[2][1])
        self.freq = _np.linspace(min(lensandlims[1][0]), max(lensandlims[1][1]), max(lensandlims[4][0]))
        self.time = _np.linspace(min(lensandlims[3][0]), max(lensandlims[3][1]), max(lensandlims[4][1]))

        # indices of lower and upper freqs in data
        bandlow = _np.searchsorted(self.freq, bandlow)
        bandhigh = _np.searchsorted(self.freq, bandhigh)
        self.bandfreq = self.freq[bandlow:bandhigh]

        twod_spec_l = []
        twod_band_spec_l = []
        twod_volt_l = []

        for index in range(len(p1)):
            if self.freqs[index, 0] is None:
                spec = _np.ones(len(self.freq)) * self.minspec  # return uniform spec at lowest power if no spec recorded
            else:
                # interpolate the spectra to be at same freq points for all jobs
                spec = _np.interp(self.freq, self.freqs[index, 0], self.specs[index, 0])
                for i, j in enumerate(spec):
                    if j < self.minspec:
                        # set to min spec as occasional element can be negative and ruins plotting on a log scale
                        spec[i] = self.minspec
            bandspec = spec[bandlow:bandhigh]

            twod_spec_l.append(spec)
            twod_band_spec_l.append(bandspec)

            if self.times[index, 0] is None:
                volt = _np.zeros(len(self.volts)) * self.minvolt # set to array of zeroes if no signal recorded
            else:
                # interpolate the signals to be at same time points for all jobs
                volt = _np.interp(self.time, self.times[index, 0], self.volts[index, 0])
            twod_volt_l.append(volt)

        self.twod_specs = _np.array(twod_spec_l)  # array of spectra for every value of param1
        self.twod_band_specs = _np.array(twod_band_spec_l)  # array of narrow band spectra for every value of param1
        self.twod_volts = _np.array(twod_volt_l)  # array of time domain signal for every value of param1


    def __repr__(self):
        s = "Data1D object, parameters varied are:\n"
        for i in self._varied:
            s += " " + i + "\n"
        return s


class Data2D(DataBase):
    def __init__(self, batchjob, param1, param2, bandlow=10e9, bandhigh=20e9):
        """ Data container for a batch job where two geometry parameters are varied.
            batchjob: wgbpm.Data.Batch instance containing necessary info for the batch job
            param1: name of parameter varied, e.g. SlW
            param2: name of parameter varied, e.g. SlH
            bandlow: lower frequency bound to select narrower region of interest (Hz)
            bandhigh: higher frequency bound to select narrower region of interest (Hz)
            """
        super(Data2D, self).__init__(batchjob)
        p1 = getattr(self, param1)
        p2 = getattr(self, param2)

        self.freqs = _np.empty((len(p1), len(p2)), dtype=_pan.core.series.Series)
        self.specs = _np.empty((len(p1), len(p2)), dtype=_pan.core.series.Series)
        self.times = _np.empty((len(p1), len(p2)), dtype=_pan.core.series.Series)
        self.volts = _np.empty((len(p1), len(p2)), dtype=_pan.core.series.Series)
        self.geoms = _np.empty((len(p1), len(p2)), dtype=dict)

        for i in self._data:
            p1ind = list(p1).index(float(i.geom[param1]))
            p2ind = list(p2).index(float(i.geom[param2]))
            self.freqs[p1ind, p2ind] = i.freq
            self.specs[p1ind, p2ind] = i.spectrum
            self.times[p1ind, p2ind] = i.time
            self.volts[p1ind, p2ind] = i.voltage
            self.geoms[p1ind, p2ind] = i.geom

        lensandlims = self.GetLimitsAndLens2D(p1, p2)  # order: spec, freq, volts, times, and lens (freqs, times)

        self.minspec = min(lensandlims[0][0])
        self.maxspec = max(lensandlims[0][1])
        self.minvolt = min(lensandlims[2][0])
        self.maxvolt = max(lensandlims[2][1])
        self.freq = _np.linspace(min(lensandlims[1][0]), max(lensandlims[1][1]), max(lensandlims[4][0]))
        self.time = _np.linspace(min(lensandlims[3][0]), max(lensandlims[3][1]), max(lensandlims[4][1]))

        bandlow = _np.searchsorted(self.freq, bandlow)
        bandhigh = _np.searchsorted(self.freq, bandhigh)
        self.bandfreq = self.freq[bandlow:bandhigh]

        # lists of arrays (of spectra and signals), one array for every value of param1 with each of those arrays
        # having the spectra for every value of param2
        self.twod_specs = []
        self.twod_band_specs = []
        self.twod_volts = []

        for p1index in range(len(p1)):
            twod_spec_l = []
            twod_band_spec_l = []
            twod_volt_l = []

            for p2index in range(len(p2)):
                if self.freqs[p1index, p2index] is None:
                    spec = _np.ones(len(self.freq)) * self.minspec  # return uniform spec at lowest power if no spec recorded
                else:
                    # interpolate the spectra to be at same freq points for all jobs
                    spec = _np.interp(self.freq, self.freqs[p1index, p2index], self.specs[p1index, p2index])
                    for i, j in enumerate(spec):
                        # set to min spec as occasional element can be negative and ruins plotting on a log scale
                        if j < self.minspec:
                            spec[i] = self.minspec

                if self.times[p1index, p2index] is None:
                    volt = _np.zeros(len(self.time)) * self.minvolt  # set to array of zeroes if no signal recorded
                else:
                    # interpolate the signals to be at same time points for all jobs
                    volt = _np.interp(self.time, self.times[p1index, p2index], self.volts[p1index, p2index])

                bandspec = spec[bandlow:bandhigh]
                twod_spec_l.append(spec)
                twod_band_spec_l.append(bandspec)
                twod_volt_l.append(volt)

            self.twod_specs.append(_np.array(twod_spec_l))
            self.twod_band_specs.append(_np.array(twod_band_spec_l))
            self.twod_volts.append(_np.array(twod_volt_l))

    def __repr__(self):
        s = "Data2D object, parameters varied are:\n"
        for i in self._varied:
            s += " " + i + "\n"
        return s


