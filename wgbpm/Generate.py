import subprocess as _sub
from string import Template as _Tem
import time as _time
import os as _os
import shutil as _shutil
import pickle as _pic
import glob as _glob
import datetime as _dt

from .General import *
from . import QuickPlot as qpl
from . import Plot as pl


def SetupRun(localdir, datadir, batchname=None, scriptPath=None, quickPlot=False, recoverDir=None,
             copyWgbpm=True, wgbpmPath="/home/wshields/wgbpm/linapp/", verbose=False):
    """ Create folders for job scripts, results, and plots.

    localdir:   the file path to the parent directory where the model will be generated and the results stored
    datadir:    the directory where the raw GdfidL data will be stored. This can be large (several GBs) so be careful
                where this directory is.
    batchname:  the directory name where all final data and results are stored, e.g. VarySlotWidth. If not specified,
                a date-time string is used instead, e.g 20200715_113225
    quickPlot:  Only plot results quickly for inspecting & diagnosing GdfidL output.
    recoverDir: In the event of the script terminating prematurely, supply the timestamped directory created in the
                original script run. By rerunning with the directory supplied, the script will assess which jobs were
                not completed and pick up where the original script was stopped. E.g "20200715_113225"
    copyWgbpm:  Copy this wgbpm library into the batch job scripts directory - useful for reproducibility. Default=True,
    wgbpmPath:  Directory where this wgbpm library / repository is located, only used if copyWgbpm=True
    verbose:    Print output to terminal throughout the setup process for debugging. Default=False

    Returns a wgbpm.Data.Batch instance that holds data about the directories used in the batch run.
    """
    if recoverDir is None:
        if batchname is None:
            now = _dt.datetime.now()
            t = now.time()
            d = now.date()
            batchstr = "{:04d}{:02d}{:02d}_{:02d}{:02d}{:02d}".format(d.year, d.month, d.day, t.hour, t.minute, t.second)
        else:
            batchstr = batchname
    else:
        batchstr = recoverDir
    Print("batch name set to:" + batchstr, verbose)

    #TODO: Check for presence of recover dir if not None

    localdir = CheckTrailingSlash(localdir)
    datadir = CheckTrailingSlash(datadir)

    resdir = localdir+'results/'
    batchresdir = resdir + batchstr  # timestamped batch directory name in the results directory
    batchdatadir = datadir + batchstr  # timestamped batch directory name in the data directory
    Print("batch results directory set to:" + batchresdir, verbose)
    Print("batch data directory set to:" + batchdatadir, verbose)

    # create object that stores all directories.
    s = Data.Batch()
    s.SetLocalDir(localdir)
    s.SetDataDir(datadir)
    s.SetResDir(resdir)
    s.SetBatchName(batchstr)

    # if recovering, just return the batch object - no need to create any folders
    if recoverDir is not None:
        Print("batch setup recovery from "+recoverDir, verbose)
        return s

    mkdir(resdir)        # make a results directory - general directory where results for many batches are stored
    mkdir(batchresdir)   # make the batch results directory (within the above resdir directory)
    mkdir(batchdatadir)  # make the batch data directory where GdfidL output is moved to upon batch job completion
    mkdir(batchresdir + "/scripts") # make the directory where copies of the scripts are stored
    mkdir(batchresdir + '/data')    # make the directory where the results are stored (pickled files)

    pl.PlotSetup(batchresdir)
    if quickPlot:
        qpl.QuickPlotSetup(batchresdir)
    Print("batch directories made", verbose)

    if copyWgbpm:
        Print("copying wgbgpm library to batch directory", verbose)
        wgbpmdir = CheckTrailingSlash(wgbpmPath)
        if _os.path.exists(wgbpmdir + "wgbpm"):
            Print("wgbpm library location: "+wgbpmdir+"wgbpm", verbose)
            Print("copying library to: " + batchresdir + "/scripts/wgbpm", verbose)
            _shutil.copytree(wgbpmdir + "wgbpm", batchresdir + "/scripts/wgbpm")
        else:
            Print("wgbpm library not found in wgbpmPath, library will not be copied in this batch job.", True)

    if scriptPath is not None:
        sp = CheckTrailingSlash(scriptPath)
        Print("using user defined script path: " + sp, verbose)
    else:
        sp = localdir
        Print("using local directory path: " + sp, verbose)
    for filename in _glob.glob(sp + "*"):
        if ".py" in filename or ".gdf" in filename:
            _shutil.copy(filename, batchresdir + "/scripts/")
    Print(".py and .gdf script files copied to " + batchresdir + "/scripts/", verbose)
    return s


def SubmitJob(jobdata, tasktime="24:00:00", excludeNodes=None, includeNodes=None, sbatchFlags=[], verbose=False):
    """ Submit a single job with slurm.
        jobdata:      wgbpm.Data.Job instance
        tasktime:     maximum job time - jobs are killed by slurm after this length of time. Format must be "HH:MM:SS".
        excludeNodes: node or list of nodes to exclude from available nodes to submit to.
        includeNodes: node or list of nodes to include in available nodes to submit to.
        sbatchFlags:  a dictionary of flags to be used in the sbatch command, e.g. ["--nodes=4","--time="01:00:00"]
        verbose:      Print output to terminal during the job submission process for debugging. Default=False
    """

    f_nodes = "--nodes=" + str(jobdata.nodes)
    f_taskspernode = "--ntasks-per-node=" + str(jobdata.taskspernode)
    f_cpuspertask = "--cpus-per-task=" + str(jobdata.cpuspertask)
    f_tasktime = "--time=" + tasktime
    f_jobname = "--job-name=" + jobdata.jobname

    subprocessCall = ["sbatch", f_nodes, f_taskspernode, f_cpuspertask, f_tasktime, f_jobname]

    if excludeNodes is not None:
        f_nodes_exclude = "--exclude="
        try:
            if not isinstance(excludeNodes, str):
                nodes = ""
                for i in excludeNodes:
                    nodes += str(i) + ","
                f_nodes_exclude += nodes
        except (IndexError, ValueError) as error:
            f_nodes_exclude += excludeNodes
        subprocessCall.append(f_nodes_exclude)

    if includeNodes is not None:
        f_nodes_include = "--nodelist="
        try:
            if not isinstance(includeNodes, str):
                nodes = ""
                for i in includeNodes:
                    nodes += str(i) + ","
                f_nodes_include += nodes
        except (IndexError, ValueError) as error:
            f_nodes_include += includeNodes
        subprocessCall.append(f_nodes_include)

    if len(sbatchFlags) > 0:
        for flag in sbatchFlags:
            subprocessCall.append(flag)
    f_script = "start.sh"
    subprocessCall.append(f_script)

    _sub.call(subprocessCall)  # the actual job submission
    s = ""
    for i in subprocessCall:
        s += i + " "
    Print("submitted "+jobdata.jobname+ " job with sbatch command:\n " + s, verbose)


def GetActiveJobs(hpcUserName="wshields"):
    """ Get number of active jobs for given username.
        hpcUserName: username as a string"""
    _os.system("squeue -u " + hpcUserName + " | wc -l > jobs.txt")  # check number of jobs, dump output to file
    numjobs = [int(line.strip()) - 1 for line in open("jobs.txt", 'r')][0]  # extract number of jobs from file
    return numjobs


def SubmitJobs(batchjob, hpcUserName="wshields", maxjobs=100, pickleFileName="batchjob.p", verbose=False):
    """ Submit multiple jobs in a wgbpm.Data.Batch instance with slurm.
        batchjob:       wgbpm.Data.batch instance with jobs instances added.
        hpcUserName:    username on hpc server, used to determine how many jobs a user has in the queue. Default=wshields
        maxjobs:        maximum number of concurrent submitted jobs. Default=100
        pickleFileName: name of the pickle file that the batch object will be saved to. Default="batchjob.p"
        verbose:        Print output to terminal during the job submission process for debugging. Default=False
    """

    def subjob(jobdata, pwd, verbose=False):
        # submit the job in the data directory and pickle jobdata instance
        _os.chdir(jobdata.datapath)
        SubmitJob(jobdata, tasktime=jobdata.tasktime, excludeNodes=jobdata.excludeNodes, includeNodes=jobdata.includeNodes, verbose=verbose)
        _pic.dump(jobdata, open(jobdata.batchinfo.batchresdir + "/data/" + jobdata.jobname + ".p", "wb"))
        _os.chdir(pwd)

    # save the batch job object to a pickle file in case things go wrong
    SaveJobs(batchjob, pickleFileName)
    Print("batch job object saved in pickled file: " + pickleFileName, verbose)

    pwd = _os.getcwd()
    # submit all the jobs with sbatch
    for jobdata in batchjob.joblist:
        if CheckJobEndedInRecovery(jobdata):
            Print("Job " + jobdata.jobname + " already completed", True)
        else:
            numjobs = GetActiveJobs(hpcUserName=hpcUserName)
            if numjobs < maxjobs:
                subjob(jobdata, pwd, verbose)
            else:
                queuespace = False
                while not queuespace:
                    numjobs = GetActiveJobs(hpcUserName=hpcUserName)
                    if numjobs >= maxjobs:
                        _time.sleep(2)
                    else:
                        queuespace = True
                subjob(jobdata, pwd, verbose)
            _time.sleep(2)


def SetupJob(jobdata, stepsize=1e-3, scriptpath=None, recoverDir=None, templatefile="template.gdf",
             copyShellScripts=True, templateDict={}, verbose=False):
    """ Setup the job. Writes gdf file and copies scripts to job data directory
        jobdata:          wgbpm.Data.Job instance
        stepsize:         mesh step size in metres. Default=1e-3.
        scriptpath:       path to the directory where the script is stored.
        recoverDir:       Timestamped directory of the job in the event of script not completing, e.g "20200715_113225"
        templatefile:     name of the templated file used to create the gdf files in each job. Default="template.gdf".
        copyShellScripts: copy the wgbpm shellscripts into the job directory. Default=True
        templateDict:     Dictionary containing variables in the template gdf file with the dict's values.
                          E.g. {"nrofthreads": 20} to replace $nrofthreads with 20 in the gdf file. This dict should not
                          contain any geometry parameters being varied, however it can contain constant values used in
                          all simulations. It also should not contain the number of threads, the outfile, or the scratchbase
        verbose:          Print output to terminal during the job setup process for debugging. Default=False
        """
    if not isinstance(jobdata, Data.Job):
        raise TypeError("job must be a wgbpm.Data.Job instance")

    datapath = jobdata.batchinfo.localdir + jobdata.jobname + "/"
    jobdata.SetDataPath(datapath)
    Print("job data path set to: " + datapath, verbose)

    # if recovering, just add the datapath and return - no need to write anything to disk
    if recoverDir is not None:
        Print("job setup recovery from "+recoverDir, verbose)
        return jobdata

    # make a local directory and copy in all the necessary files
    if jobdata.jobname not in _os.listdir(jobdata.batchinfo.localdir):
        mkdir(datapath)
    Print("job data path set to: " + datapath, verbose)

    if scriptpath is None:
        sourcepath = "/home/wshields/wgbpm/linapp/template.gdf"
    else:
        sp = CheckTrailingSlash(scriptpath)
        sourcepath = sp + templatefile
    Print("template script path set to: " + sourcepath, verbose)
    _shutil.copy(sourcepath, jobdata.batchinfo.batchresdir)
    _shutil.copy(sourcepath, datapath)
    Print("template script copied to batch results directory: " + jobdata.batchinfo.batchresdir, verbose)
    Print("template script copied to job data directory: " + datapath, verbose)

    if copyShellScripts:
        # todo: mechanism to copy custom shellscripts to data directory
        for filename in _glob.glob(_os.path.join("/home/wshields/wgbpm/local/shellscripts/", '*')):
            _shutil.copy(filename, datapath)
        Print("shellscripts copied to job data directory: " + datapath, verbose)

    # get data to to written to the gdfidl input file
    variable_dict = {"stepsize": str(stepsize)}
    # extend with geometry params
    for key in jobdata.geom.keys():
        variable_dict[key] = jobdata.geom[key]
    # extend with new directory and num threads
    template_file = datapath + templatefile
    variable_dict["nthreads"] = str(jobdata.nthreads)
    variable_dict["outfile"] = datapath[:-1]
    variable_dict["scratchbase"] = datapath + jobdata.jobname + "-"

    # update template variable dict with user variables
    if templateDict != {}:
        variable_dict.update(templateDict)

    # write the gdfidl input file using the template
    with open(template_file, "r+") as f:
        template_contents = f.read()
    template = _Tem(template_contents)

    template_resolved = template.safe_substitute(variable_dict)

    output_file = datapath + "wgTest.gdf"
    with open(output_file, "w") as f:
        f.write(template_resolved)
    Print("job template file written: " + output_file, verbose)
    return jobdata


def WritePostScript(jobdata):
    """ Write the post processing script.
        jobdata: wgbpm.Data.Job instance """
    # TODO: use external file instead of writing hard coded script
    poststr = "-general infile " + jobdata.datapath + "\n"
    poststr += "-spara\n"
    poststr += "flow 150e6\n"
    poststr += "fhigh 200e9\n"
    poststr += "windowed no\n"
    poststr += "onlyplotfiles yes\n"
    poststr += "\n"
    poststr += "time yes, do\n"
    poststr += "showeh no, do\n"
    f = open('post', 'w')
    f.write(poststr)
    f.close()


def PostProcessFile(pfile, batchjob, postscript='', gdfidlTmax=15, saveDataInResults=True, verbose=False):
    """ Run post processing script wth gd1.pp. Reads time and freq domain data
        and adds to pickled jobdata object.
        pfile:             path to pickled jobdata file
        postscript:        path to user supplied postscript file. Default file written out in case of no file supplied
        gdfidlTmax:        maximum time (s) to wait for checking all main GdfidL output has been written to disk
        saveDataInResults: save a copy of the raw time and freq domain data with the results (separate to full data
                           copy stored in user supplied data directory)
        verbose:           Print output to terminal during the post processing process for debugging. Default=False
        """
    jobdata = LoadPickleData(pfile)
    picklefile = jobdata.batchinfo.batchresdir + "/data/" + jobdata.jobname + ".p"

    pwd = _os.getcwd()
    # TODO: check for existence of data path
    _os.chdir(jobdata.datapath)
    # wait for it to run and check for gdfidl file
    time_counter = 0
    tstep = 5
    while not _os.path.exists(jobdata.datapath + "WHAT-GDFIDL-DID-SPIT-OUT"):
        _time.sleep(tstep)
        time_counter += tstep
        if time_counter > gdfidlTmax:
            Print("GDFIDL files not written out", True)
            return None

    # write and run postprocessing script
    if postscript == '':
        WritePostScript(jobdata)
        _os.system("gd1.pp < post > postlog.txt")
        Print("post-processing file 'post' written in: "+jobdata.datapath, verbose)
        Print("using post-processing file 'post'", verbose)
    else:
        _os.system("gd1.pp < " + postscript + " > postlog.txt")
        Print("using post-processing file " + postscript, verbose)

    _time.sleep(2)  # allow time for pp job to finish

    # load data and extract freq. spectrum
    sigoutfile = jobdata.batchinfo.fdomainDataFile
    timeoutfile = jobdata.batchinfo.tdomainDataFile
    if batchjob.fdomainDataFile != sigoutfile:
        sigoutfile = batchjob.fdomainDataFile
        timeoutfile = batchjob.tdomainDataFile

    time_counter = 0
    while not _os.path.exists(sigoutfile):
        _time.sleep(2)
        time_counter += tstep
        if time_counter > tstep:
            Print("Post processing files not written out yet, waiting...", True)
        if time_counter > gdfidlTmax*2:
            Print("Cannot find postprocessing output file for job " + jobdata.jobname, True)
            return None

    t,v = LoadFile(timeoutfile, skiprows=10)
    jobdata.AddTime(t)
    jobdata.AddVoltage(v)
    Print("time domain data loaded from file " + timeoutfile, verbose)

    f,s = LoadFile(sigoutfile, skiprows=8)
    jobdata.AddFreq(f)
    jobdata.AddSpectrum(s)
    Print("frequency domain data loaded from file " + sigoutfile, verbose)

    if jobdata is not None:
        _pic.dump(jobdata, open(picklefile, "wb"))
        Print("saving job object in pickle file " + picklefile, verbose)

    if saveDataInResults:
        resultsDataDir = jobdata.datapath+"/" + jobdata.jobname
        mkdir(resultsDataDir)
        _shutil.copy(sigoutfile, resultsDataDir)
        _shutil.copy(timeoutfile, resultsDataDir)
        Print("raw time & freq data copied to results data directory " + resultsDataDir, verbose)

    _os.chdir(pwd)

    # move data from output dir to data dir
    _shutil.move(jobdata.datapath, jobdata.batchinfo.batchdatadir)
    Print("GfdidL output data moved to " + jobdata.batchinfo.batchdatadir, verbose)

    Print("Processed file: " + jobdata.jobname, True) # always print


def PostProcessFiles(batchjob, postscript='', gdfidlTmax=15, saveDataInResults=True, verbose=False):
    """ Run post processing script wth gd1.pp for all jobs in a batch.
        batchjob:          wgbpm.Data.Batch object containing job instances
        postscript:        path to user supplied postscript file. Default file written out in case of no file supplied
        gdfidlTmax:        maximum time (s) to wait for checking all main GdfidL output has been written to disk
        saveDataInResults: save a copy of the raw time and freq domain data with the results (separate to full data
                           copy stored in user supplied data directory)
        verbose:           Print output to terminal during the post processing process for debugging
    """
    files = GetPickledData(batchjob)
    for pickledfile in files:
        Print("Loading file: " + pickledfile, verbose)
        PostProcessFile(pickledfile, batchjob, postscript=postscript, gdfidlTmax=gdfidlTmax,
                        saveDataInResults=saveDataInResults, verbose=verbose)

