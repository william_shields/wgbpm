import os
import numpy as _np
import sys
sys.path.append("/Users/williamshields/Physics/wgbpm/local/")
sys.path.append("/home/wshields/wgbpm/linapp/wgbpm/")

import wgbpm.Data as Data
import wgbpm.Generate as Gen
import wgbpm.QuickPlot as qpl

# Set the local directory where this script is (and where the results and plots will be saved)
# and the data directory where the GdfidL output will be stored.
localdir = "/home/wshields/wgbpm/local/wgBpm/"
datadir = "/home/wshields/wgbpm/data/wgBpm"

# Run the job setup - returns batch instance with batch info
batchjob = Gen.SetupRun(localdir, datadir)

# Fixed Geometry - waveguide in this case is set to WR62
a = 8.0e-3        # beam pipe radius
WgW = 0.0157988   # width of the waveguide
WgH = 0.0078994   # height of the waveguide
WgL = 60.0e-3     # length of mesh beyond beam pipe radius (1x in +/- X and +/- Y, 2x in +/- Z)
SlOff = 1.0e-3    # offset from the beam pipe (depth)
SlWall = 0.5e-3   # wall thickness  - fixed (>= 0.5mm)
WgOff = 11.5e-3   # waveguide offset from the beam pipe
CutL = 0.007      # coupling window cut-out length

# variable Geometry
SlW = 11.0e-3     # width of the coupling slot - default value for reference
SlH = 2.0e-3      # height of the coupling slot - default value for reference

# Set the parameter and values that are varied in the jobs
param1Name = 'SlW' # needed later in data loading, string should be exactly the same as an argument supplied to
                     # the Geom instance later
slwvalues = _np.linspace(10.0e-3, 11.0e-3, 2)
param2Name = 'SlH' # needed later in data loading, string should be exactly the same as an argument supplied to
                     # the Geom instance later
slhvalues = _np.linspace(1.0e-3, 2.0e-3, 2)

param1Desc = "Coupling Slot Width (mm)"  # description of the first varied parameter - will be used as plot axis label.

# hpc job submission settings & GdfidL mesh step size
nodes = 1
taskspernode = 1
cpuspertask = 8
tasktime = "24:00:00"
stepsize = 0.2e-3

# Create Job objects - contains all necessary info about the job.
# For each job that will be run, you need to:
#   1. Create a Geom instance which is a container for the geometry parameters for that specific job
#   2. Set a unique job name for that job, it would typically relate to the unique geometry parameter and value.
#      It must be unique - it will be the output file name for that job which will be overwritten if used
#      by multiple jobs
#   3. Create a Job instance which will contain the Geom and Structure instances, as well as the job submission
#      settings and eventually the data output from GdfidL
#   4. Append the Job instance to the job list

for var1 in slwvalues:
    for var2 in slhvalues:
        # set the geometry for this specific job. Two of these arguments should be the paramXVaried from earlier
        geom = Data.Geom(a=a, WgW=WgW, WgH=WgH, WgOff=WgOff, WgL=WgL,
                         SlWall=SlWall, SlW=var1, SlH=var2, SlOff=SlOff, CutL=CutL)

        # unique job name include values of the two varied parameters
        jobname = param1Name+'_' + '%.4f' % var1 + '_' + param2Name + '_' + '%.4f' % (var2)

        # create the Job instance
        jobdata = Data.Job(geom, batchjob, jobname, nodes, taskspernode, cpuspertask, tasktime)
        jobdata = Gen.SetupJob(jobdata, stepsize)
        batchjob.AddJob(jobdata)

# submit all jobs and wait until all have completed.
Gen.SubmitJobs(batchjob)
Gen.WaitForJobsToEnd()

# postprocess all jobs - data added to pickled file
Gen.PostProcessFiles(batchjob)

# load all
os.chdir(batchjob.batchresdir)
data = Data.Data2D(batchjob, param1Name, param2Name)

#plotting for varying 2 parameters

param1 = getattr(data, param1Name)
param2 = getattr(data, param2Name)

qpl.AllSpectra2Params(data, param1, param2, param1Name, param2Name)
qpl.AllSpectra2Params(data, param1, param2, param1Name, param2Name, xscale='log', yscale='log')

qpl.AllBandSpectra2Params(data, param1, param2, param1Name, param2Name)
qpl.AllBandSpectra2Params(data, param1, param2, param1Name, param2Name, xscale='log', yscale='log')

qpl.AllSpectraVsParameter(data, param2, param1, param1Name, ylabel=param1Desc)
qpl.AllSpectraVsParameterBandAndSample(data, param2, param1, param1Name, ylabel=param1Desc)

qpl.AllSignals2Params(data, param1, param2, param1Name, param2Name)
qpl.AllSignalsVsParameter(data, param2, param1, param1Name)

