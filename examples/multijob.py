import sys
sys.path.append("/Users/williamshields/Physics/wgbpm/local/")
sys.path.append("/home/wshields/wgbpm/linapp/wgbpm/")
import os
import numpy as _np
from wgbpm.General import WriteTemplatedFile, LoadTestedValues, WriteValsAsTested
import random

# local directory where this script is and where the looped scripts and associated output files will be written,
# and data directory where the raw GdfidL output data will be stored
localdir = "/home/wshields/wgbpm/local/tolerances/"
datadir = "/home/wshields/wgbpm/data/tolerances/"

# file containing record of varied params values already been tested - shouldn't rerun same tests multiple times
testedValsFile = "testedTolerances.dat"

# file name and path of the template used to generate python scripts
template_file = localdir + "jobtemplate.dat"

# The values of the varied parameters that will be set in each job
SlXoff = _np.linspace(-0.001,0.001,11)
SlRotX = _np.linspace(-3,3,7)

numRandomSims = 5

for index, cutl in enumerate(range(numRandomSims)):
    ranOffsetindex = random.randrange(0, len(SlXoff)-1, 1)
    ranRotindex = random.randrange(0, len(SlRotX)-1, 1)

    ranOffset = SlXoff[ranOffsetindex]
    ranRot = SlRotX[ranRotindex]
    values = [ranOffset, ranRot]

    testedVals = LoadTestedValues(testedValsFile)
    if values in testedVals:
        continue
    else:
        WriteValsAsTested(testedValsFile, values)

        # collate data to be written to the gdfidl input file
        variable_dict = {"xoffset": ranOffset,
                         "xrot": ranRot}

        # set the unique file name for each python script that will be written
        output_script = localdir + "job_" + str(index) + ".py"

        # write the python script with the unique cutl value
        WriteTemplatedFile(template_file, output_script, variable_dict)

        # run the newly generated python script
        os.system("python " + output_script)

        # delete script - a copy should have been made after each run
        os.remove(output_script)

