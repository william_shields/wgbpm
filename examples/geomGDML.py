import pyg4ometry.gdml as _gd
import pyg4ometry.geant4 as _g4
import pyg4ometry.visualisation as _vi
import sys
sys.path.append("/Users/williamshields/Physics/wgbpm/local/")
sys.path.append("/home/wshields/wgbpm/linapp/")


# Fixed Geometry - waveguide set to WR62
# a = 8.0e-3        # beam pipe radius
# WgW = 0.0157988   # width of the waveguide
# WgH = 0.0078994   # height of the waveguide
# WgL = 60.0e-3     # length of mesh beyond beam pipe radius (1x in +/- X and +/- Y, 2x in +/- Z)
#
# # variable Geometry
# WgOff = 11.5e-3   # waveguide offset from the beam pipe
#
# SlW = 22.9e-3     # width of the coupling slot
# SlH = 5.0e-3      # height of the coupling slot
# SlOff = 2.0e-3    # offset from the beam pipe (depth)
# SlWall = 0.5e-3   # wall thickness  - fixed (>= 0.5mm)
# CutL = 2.0e-3    # coupling window cut-out length

vis = False
interactive = False
n_slice = 30

reg = _g4.Registry()

# defines
infin = _gd.Constant("infin", "200", reg, True)
a = _gd.Constant("a", "8", reg, True)
wgw = _gd.Constant("WgW", "15.7988", reg, True)
wgh = _gd.Constant("WgH", "7.8994", reg, True)
wgl = _gd.Constant("WgL", "60", reg, True)
wgoff = _gd.Constant("WgOff", "11.5", reg, True)

slw = _gd.Constant("SlW", "11", reg, True)
slh = _gd.Constant("SlH", "5.5", reg, True)
sloff = _gd.Constant("SlOff", "10", reg, True)
slwall = _gd.Constant("SlWall", "0.5", reg, True)
cutl = _gd.Constant("CutL", "7", reg, True)

trmin = _gd.Constant("trmin", "2.5", reg, True)
trmax = _gd.Constant("trmax", "10.0", reg, True)
tz = _gd.Constant("tz", "50", reg, True)
tstartphi = _gd.Constant("startphi", "0", reg, True)
tdeltaphi = _gd.Constant("deltaphi", "2.0*pi", reg, True)
halfpi = _gd.Constant("halfpi", "0.5*pi", reg, True)

# materials
wm = _g4.Material(name="G4_Galactic")
cu = _g4.Material(name="G4_Cu")

# world
ws = _g4.solid.Box("ws", infin, infin, infin, reg, "mm")
wl = _g4.LogicalVolume(ws, wm, "wl", reg)

# solids
bpm_s = _g4.solid.Box("bpm_s", infin/3, infin/3, infin/6, reg, "mm")

# first vacuum beam pipe - at world centre
t1_s = _g4.solid.Tubs("t1s", 0, a, infin/2.0, tstartphi, tdeltaphi, reg, "mm", "rad", nslice=n_slice)

# pickup piece of the beam pipe
pu_len = slw + slwall + cutl
pu_origin = -slw/2.0 + cutl
pu_sub = _g4.solid.Tubs("pu_sub", a - slwall, a, pu_len/2.0, tstartphi, tdeltaphi, reg, "mm", "rad", nslice=n_slice)

# subtract the ring with correct inner radius
pos = pu_origin + pu_len/2.0
s2 = _g4.solid.Subtraction('s2', t1_s, pu_sub, [[0, 0, 0], [0, 0, pu_origin + pu_len/2.0]], reg)




cslen = infin - (a - sloff)
cs_s = _g4.solid.Box("css", cslen, slh, slw, reg, "mm")
wg_s = _g4.solid.Box("wgs", infin - (a + wgoff), wgh, wgw, reg, "mm")

pos = (a - sloff) + wgoff
port = _g4.solid.Union('port', cs_s, wg_s, [[0, 0, 0], [pos, 0, 0]], reg)
port_lv = _g4.LogicalVolume(port, wm, "port_lv", reg)

p1 = _g4.solid.Union('p1', s2, port, [[0, 0, 0], [(a - sloff) + cslen/2, 0, 0]], reg)
p2 = _g4.solid.Union('p2', p1, port, [[0, 0, 2.0*halfpi], [-((a - sloff) + cslen/2), 0, 0]], reg)
p3 = _g4.solid.Union('p3', p2, port, [[0, 0, 1.0*halfpi], [0, (a - sloff) + cslen/2, 0]], reg)
p4 = _g4.solid.Union('p4', p3, port, [[0, 0, 3.0*halfpi], [0, -((a - sloff) + cslen/2), 0]], reg)

bpm = _g4.solid.Subtraction('bpm', bpm_s, p4, [[0, 0, 0], [0, 0, 0]], reg)

pu_s = _g4.solid.Tubs("pu_s", a - slwall, a, pu_len/2.0, tstartphi, tdeltaphi, reg, "mm", "rad", nslice=n_slice)

bpm_ppu = _g4.solid.Union("bpm_ppu_s", bpm, pu_s, [[0, 0, 0], [0, 0, pu_origin + pu_len/2.0]], reg)

cutaway_s = _g4.solid.Box("cutaway_s", infin/3, infin/3, infin/3, reg, "mm")

fin = _g4.solid.Subtraction("fin_s", bpm_ppu, cutaway_s, [[0, 0, 0], [-infin/6, infin/6, 0]], reg)
fin_lv = _g4.LogicalVolume(fin, wm, "fin_lv", reg)
fin_pv = _g4.PhysicalVolume([0, 0, 0], [0, 0, 0], fin_lv, "fin_pv", wl, reg)



# set world volume
reg.setWorld(wl.name)

# test extent of physical volume
wlextent = wl.extent(True)
wlextent_daughters = wl.extent(False)

# test extent of physical volume
extentBB = wl.extent(includeBoundingSolid=True)
extent = wl.extent(includeBoundingSolid=False)

# visualisation
v = _vi.VtkViewer()
v.addLogicalVolume(reg.getWorldVolume())
#v.addAxes(_vi.axesFromExtents(extentBB)[0])
v.view(interactive=interactive)


