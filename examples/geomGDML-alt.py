import pyg4ometry.gdml as _gd
import pyg4ometry.geant4 as _g4
import pyg4ometry.visualisation as _vi
import sys
sys.path.append("/Users/williamshields/Physics/wgbpm/local/")
sys.path.append("/home/wshields/wgbpm/linapp/")

vis = False
interactive = False
n_slice = 30

reg = _g4.Registry()

# defines
infin = _gd.Constant("infin", "200", reg, True)
a = _gd.Constant("a", "8", reg, True)
wgw = _gd.Constant("WgW", "15.7988", reg, True)
wgh = _gd.Constant("WgH", "7.8994", reg, True)
wgl = _gd.Constant("WgL", "60", reg, True)
wgoff = _gd.Constant("WgOff", "11.5", reg, True)

slw = _gd.Constant("SlW", "11", reg, True)
slh = _gd.Constant("SlH", "5.5", reg, True)
sloff = _gd.Constant("SlOff", "2", reg, True)
slwall = _gd.Constant("SlWall", "0.5", reg, True)
cutl = _gd.Constant("CutL", "7", reg, True)

trmin = _gd.Constant("trmin", "2.5", reg, True)
trmax = _gd.Constant("trmax", "10.0", reg, True)
tz = _gd.Constant("tz", "50", reg, True)
tstartphi = _gd.Constant("startphi", "0", reg, True)
tdeltaphi = _gd.Constant("deltaphi", "2.0*pi", reg, True)
halfpi = _gd.Constant("halfpi", "0.5*pi", reg, True)

# materials
wm = _g4.Material(name="G4_Galactic")
cu = _g4.Material(name="G4_Cu")

# world
ws = _g4.solid.Box("ws", infin, infin, infin, reg, "mm")
wl = _g4.LogicalVolume(ws, wm, "wl", reg)

# bpm solid block
bpm_s = _g4.solid.Box("bpm_s", infin/3, infin/3, 50, reg, "mm")

# first vacuum beam pipe - at world centre
t1_s = _g4.solid.Tubs("t1s", 0, a, infin/2.0, tstartphi, tdeltaphi, reg, "mm", "rad", nslice=n_slice)

v1_s = _g4.solid.Subtraction('v1_s', bpm_s, t1_s, [[0, 0, 0], [0, 0, 0]], reg)

pu_len = slw + slwall - cutl
pu_origin = -slw/2.0 - slwall
pu_s = _g4.solid.Tubs("pu_s", a - sloff - slwall, a, pu_len, tstartphi, tdeltaphi, reg, "mm", "rad", nslice=n_slice)

bpm_ppu = _g4.solid.Union("bpm_ppu_s", v1_s, pu_s, [[0, 0, 0], [0, 0, pu_origin + pu_len/2.0]], reg)


cslen = infin - (a - sloff)
cs_s = _g4.solid.Box("css", cslen, slh, slw, reg, "mm")
wg_s = _g4.solid.Box("wgs", infin - (a + wgoff), wgh, wgw, reg, "mm")

pos = (a - sloff) + wgoff
port = _g4.solid.Union('port', cs_s, wg_s, [[0, 0, 0], [pos, 0, 0]], reg)

p1 = _g4.solid.Subtraction('p1', bpm_ppu, port, [[0, 0, 0], [(a - sloff) + cslen/2, 0, 0]], reg)
p2 = _g4.solid.Subtraction('p2', p1, port, [[0, 0, 2.0*halfpi], [-((a - sloff) + cslen/2), 0, 0]], reg)
p3 = _g4.solid.Subtraction('p3', p2, port, [[0, 0, 1.0*halfpi], [0, (a - sloff) + cslen/2, 0]], reg)
p4 = _g4.solid.Subtraction('p4', p3, port, [[0, 0, 3.0*halfpi], [0, -((a - sloff) + cslen/2), 0]], reg)

bpm_s2 = _g4.solid.Box("bpm_s2", infin/3-1, infin/3-1, 50-1, reg, "mm")

#fin = _g4.solid.Subtraction("fin_s", bpm_s2, p4, [[0, 0, 0], [0, 0, 0]], reg)


cutaway_s = _g4.solid.Box("cutaway_s", infin/3, infin/3, infin/3, reg, "mm")

fin = _g4.solid.Subtraction("fin_s", p4, cutaway_s, [[0, 0, 0], [infin/6, infin/6, 0]], reg)
fin_lv = _g4.LogicalVolume(fin, wm, "fin_lv", reg)
fin_pv = _g4.PhysicalVolume([0, 0, 0], [0, 0, 0], fin_lv, "fin_pv", wl, reg)

# set world volume
reg.setWorld(wl.name)

# test extent of physical volume
wlextent = wl.extent(True)
wlextent_daughters = wl.extent(False)

# test extent of physical volume
extentBB = wl.extent(includeBoundingSolid=True)
extent = wl.extent(includeBoundingSolid=False)

# visualisation
v = _vi.VtkViewer()
v.addLogicalVolume(reg.getWorldVolume())
#v.addAxes(_vi.axesFromExtents(extentBB)[0])
v.view(interactive=interactive)


