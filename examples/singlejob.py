import os
import numpy as _np
import sys
sys.path.append("/Users/williamshields/Physics/wgbpm/local/")
sys.path.append("/home/wshields/wgbpm/linapp/wgbpm/")

import wgbpm.Data as Data
import wgbpm.Generate as Gen
import wgbpm.QuickPlot as qpl
import wgbpm.Plot as pl

# Set the local directory where this script is (and where the results and plots will be saved)
# and the data directory where the GdfidL output will be stored.
localdir = "/home/wshields/wgbpm/local/wgBpm/"
datadir = "/home/wshields/wgbpm/data/wgBpm"

# Run the job setup - returns batch instance with batch info
batchjob = Gen.SetupRun(localdir, datadir)

# Fixed Geometry - waveguide in this case is set to WR62
a = 8.0e-3        # beam pipe radius
WgW = 0.0157988   # width of the waveguide
WgH = 0.0078994   # height of the waveguide
WgL = 60.0e-3     # length of mesh beyond beam pipe radius (1x in +/- X and +/- Y, 2x in +/- Z)
SlOff = 1.0e-3    # offset from the beam pipe (depth)
SlWall = 0.5e-3   # wall thickness  - fixed (>= 0.5mm)
WgOff = 11.5e-3   # waveguide offset from the beam pipe
CutL = 0.007      # coupling window cut-out length
SlH = 2.0e-3      # height of the coupling slot

# variable Geometry
SlW = 11.0e-3     # width of the coupling slot - default value for reference

# Set the parameter and values that are varied in the jobs
paramVaried = 'SlW' # needed later in data loading, string should be exactly the same as an argument supplied to
                    # the Geom instance later
slwvalues = _np.linspace(10.0e-3, 11.0e-3, 2)
paramDesc = "Slot Width (mm)" # description of the parameter being varied - will be used as plot axis label.

# hpc job submission settings & GdfidL mesh step size
nodes = 1
taskspernode = 1
cpuspertask = 8
tasktime = "24:00:00"
stepsize = 0.2e-3
excludeNodes="compute001"
#includeNodes=["compute002","compute003"]

# Create Job objects - contains all necessary info about the job.
# For each job that will be run, you need to:
#   1. Create a Geom instance which is a container for the geometry parameters for that specific job
#   2. Set a unique job name for that job, it would typically relate to the unique geometry parameter and value.
#      It must be unique - it will be the output file name for that job which will be overwritten if used
#      by multiple jobs
#   3. Create a Job instance which will contain the Geom and Structure instances, as well as the job submission
#      settings and eventually the data output from GdfidL
#   4. Append the Job instance to the job list

for var in slwvalues:
    # set the geometry for this specific job. One of these arguments should be the same as paramVaried from earlier
    geom = Data.Geom(a=a, WgW=WgW, WgH=WgH, WgOff=WgOff, WgL=WgL,
                     SlWall=SlWall, SlW=var, SlH=SlH, SlOff=SlOff, CutL=CutL)

    jobname = paramVaried +'_' + '%.5f' % var  # name as submitted to slurm

    # create the Job instance
    jobdata = Data.Job(geom, batchjob, jobname, nodes, taskspernode, cpuspertask, tasktime, excludeNodes=excludeNodes)
    jobdata = Gen.SetupJob(jobdata, stepsize)
    batchjob.AddJob(jobdata)

# submit all jobs and wait until all have completed.
Gen.SubmitJobs(batchjob)
Gen.WaitForJobsToEnd()

# postprocess all jobs - data added to pickled file
Gen.PostProcessFiles(batchjob)

# load all
os.chdir(batchjob.batchresdir)
data = Data.Data1D(batchjob, paramVaried)

paramData = getattr(data,paramVaried)  # data will have dynamically set attributes for all geometry parameters

# basic spectral plots - plot individual full and band spectra for each value of varied geom param
qpl.AllSpectra1Param1D(data, paramData, paramVaried)
qpl.AllBandSpectra1Param1D(data, paramData, paramVaried)
qpl.AllSpectra1Param1D(data, paramData, paramVaried, yscale='log')
qpl.AllBandSpectra1Param1D(data, paramData, paramVaried, yscale='log')

# basic spectral plots - plot individual signals for each value of varied geom param
qpl.AllSignals1Param1D(data, paramData, paramVaried)

# better full and band spectral plots
pl.AllSpectra1Param1D(data, paramData, paramVaried)
pl.AllSpectra1Param1D(data, paramData, paramVaried, xscale='log')
pl.AllSpectra1Param1D(data, paramData, paramVaried, yscale='log')
pl.AllSpectra1Param1D(data, paramData, paramVaried, xscale='log', yscale='log')
pl.AllBandSpectra1Param1D(data, paramData, paramVaried)

#  plot grid of spectra and signals subplots, one spectrum/signal per subplot. default of nine subplots per figure.
pl.SpectrumGrid(data, paramVaried, paramData, xscale='lin', yscale='lin')
pl.SignalGrid(data, paramVaried, paramData)

# 2d plots of spectrum or signal vs the varied parameter
pl.SpectrumVsParameter(data, paramData, ylabel=paramDesc)
pl.SignalVsParameter(data, paramData, ylabel=paramDesc)

# quick plots of spectrum or signal vs the varied parameter
qpl.SpectrumVsParameter(data, paramData, outfile=paramVaried, ylabel=paramDesc)
qpl.SpectrumVsParameterBandAndSample(data, paramData, outfile=paramVaried, ylabel=paramDesc)
qpl.SignalVsParameter(data, paramData, outfile=paramVaried, ylabel=paramDesc)

